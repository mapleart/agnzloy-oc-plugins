<?php
ignore_user_abort(true);
set_time_limit(0);
header('Content-Type: application/json; charset=utf-8');
////
////

$command = 'python2 ' . __DIR__ . '/cpatl_parser.py';
$command = escapeshellcmd($command);
$output = shell_exec($command);

$json = @json_decode($output, true);
if(is_array($json) && $json['success']) {
    echo $output;
    die();
}

echo json_encode(['success'=>0, 'msg'=>'Error system', 'command'=>$command]);