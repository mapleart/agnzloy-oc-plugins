<?php


// Heading Goes here:
$_['heading_title'] = 'Integration with cpa.tl ';
$_['cpatl_title'] = 'Integration with cpa.tl ';

// Text
$_['text_module'] = 'Modules';
$_['text_success'] = 'Settings saved successfully';
$_['text_notice'] = 'Attention! The product model in the SPA must match the id in the cpa upload';
$_['coatl_base_settings'] = 'Connection Settings';
$_['cpatl_extended_settings'] = 'Advanced Settings';

$_['coatl_url'] = 'Spatial address';
$_['cpanel_api geo'] = 'Geo zone for api';


// Errors
$_['error_permission'] = 'You don\'t have enough rights to change the module settings';