<h3>Основные настройки</h3>

<div class="form-group">

    <?php if($file_data && $parsed_file) { ?>

        <div class="alert alert-success">
            <h5>Файл найден</h5>
            <p>Количество офферов в файле: <?php echo $parsed_file['count_offers']; ?> </p>
            <p>Время жизни файла: <?php echo $parsed_file['live']; ?> мин. </p>
            <p>Когда был создан (по мск): <?php echo $parsed_file['time']; ?> мин. </p>

            <?php if($file_data['countries']) { ?>
                <p>В таблице отсутствуют страны <?php echo implode(',', $file_data['countries'] ); ?> </p>
            <?php } ?>
            <br>

            <a href="<?php echo $linkFileUpdate ?>" target="_blank" class="btn btn-success">Обновить</a>
        </div>

    <?php } else { ?>
        <div class="alert alert-danger">
            Файл с офферами не найден
        </div>
    <?php } ?>
</div>

<div class="form-group">
    <label for="cpatl_apigeo"> GEO магазина</label><br>
    <select id="cpatl_apigeo"  name="cpatl_apigeo" class="form-control" >
        <?php foreach ($countries as $country) { ?>
            <option <?php echo (isset($saved_settings['cpatl_apigeo']) && $saved_settings['cpatl_apigeo'] == $country['code'] ?  'selected' : '') ?>  value="<?= $country['code'] ?>"><?= $country['code']; ?>  - <?= $country['name']; ?> [<?= $country['currency']; ?>]</option>
        <?php } ?>
    </select>
    <p class="note">Выберите страну и валюту</p>
</div>


<div class="form-group">
    <label for="cpatl_webmasterid">ID вебмастера</label><br>
    <input id="cpatl_webmasterid" type="text" name="cpatl_webmasterid" class="form-control" value="<?php if (isset($saved_settings['cpatl_webmasterid'])): echo $saved_settings['cpatl_webmasterid']; endif;?>">
    <p class="note">ID вебмастера</p>
</div>

<div class="form-group">
    <label for="cpatl_webmasterapi">API key</label><br>
    <input id="cpatl_webmasterapi" type="text" name="cpatl_webmasterapi" class="form-control" value="<?php if (isset($saved_settings['cpatl_webmasterapi'])): echo $saved_settings['cpatl_webmasterapi']; endif;?>">
    <p class="note">API key</p>
</div>


<hr>

<?php if (isset($saved_settings['cpatl_apigeo']) && $saved_settings['cpatl_apigeo'] != '') { ?>

    <?php if (!empty($cpatl_errors)) { ?>
        <?php foreach($cpatl_errors as $cpatl_error) { ?>
            <div class="alert alert-danger"><?php echo $cpatl_error ?></div>
        <?php } ?>
    <?php } ?>


    <h3>Расширенные настройки</h3>


    <div class="form-group">
        <label for="cpatl_replace_model">Заменять префикс/постфикс в модели товара</label><br>
        <input id="cpatl_replace_model" type="text" name="cpatl_replace_model" class="form-control" value="<?php if (isset($saved_settings['cpatl_replace_model'])): echo $saved_settings['cpatl_replace_model']; endif;?>">
        <p class="note">Текст который нужно удалить из модели для синхронизации</p>
    </div>


    <?php if (!empty($new_offers)) { ?>
        <h3>Новые товары</h3>
        <?php echo $new_offers; ?>
    <?php } ?>


    <?php if (!empty($offers_price_diff)) { ?>
        <h3>Устаревшая цена</h3>
        <?php echo $offers_price_diff; ?>
    <?php } ?>


    <div class="form-group">
        <label for="cpatl_exceptions">ИД офферов которые нужно исключить из рассылки и импорта</label><br>
        <input id="cpatl_exceptions" type="text" name="cpatl_exceptions" class="form-control" value="<?php if (isset($saved_settings['cpatl_exceptions'])): echo $saved_settings['cpatl_exceptions']; endif;?>">
        <p class="note">Введите ID офферов через запятую</p>
    </div>


    <h3>Импорт</h3>



    <div class="form-group row">
        <label class="col-sm-2 control-label">Исключить категории</label>
        <div class="col-sm-10">
            <div class="well well-sm" style="height: 150px; overflow: auto;">
                <?php foreach ($categories as $catgory) { ?>
                    <div class="checkbox">
                        <label>
                            <?php if (in_array($catgory, $selectedCategories)) { ?>
                                <input type="checkbox" name="cpatl_categories[]" value="<?php echo $catgory; ?>" checked="checked" />
                                <?php echo $catgory; ?>
                            <?php } else { ?>
                                <input type="checkbox" name="cpatl_categories[]" value="<?php echo $catgory; ?>" />
                                <?php echo $catgory; ?>
                            <?php } ?>
                        </label>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-sm-2 control-label" for="input-parent">Категория для загрузки</label>
        <div class="col-sm-10">
            <input type="text" name="cpatl_input_category" value="<?php echo $cpatl_input_category; ?>" placeholder="" id="input-parent" class="form-control" />
            <input type="hidden" name="cpatl_import_category" value="<?php echo $cpatl_import_category; ?>" />
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 control-label">Магазины для загрузки</label>
        <div class="col-sm-10">
            <div class="well well-sm" style="height: 150px; overflow: auto;">
                <div class="checkbox">
                    <label>
                        <?php if (in_array(0, $selectedStores)) { ?>
                            <input type="checkbox" name="cpatl_store[]" value="0" checked="checked" />
                            Основной магазин
                        <?php } else { ?>
                            <input type="checkbox" name="cpatl_store[]" value="0" />
                            Основной магазин
                        <?php } ?>
                    </label>
                </div>
                <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                        <label>
                            <?php if (in_array($store['store_id'], $selectedStores)) { ?>
                                <input type="checkbox" name="cpatl_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                                <?php echo $store['name']; ?>
                            <?php } else { ?>
                                <input type="checkbox" name="cpatl_store[]" value="<?php echo $store['store_id']; ?>" />
                                <?php echo $store['name']; ?>
                            <?php } ?>
                        </label>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <div class="form-group">
        <label for="cpatl_quantity">Количество товара</label><br>
        <input id="cpatl_quantity" type="text" name="cpatl_quantity" class="form-control" value="<?php  echo ( isset($saved_settings['cpatl_quantity']) ? $saved_settings['cpatl_quantity'] : 9999); ?>">
    </div>

<script type="text/javascript"><!--
    $('input[name=\'cpatl_input_category\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    json.unshift({
                        category_id: 0,
                        name: 'Выберите категорию'
                    });

                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['category_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'cpatl_input_category\']').val(item['label']);
            $('input[name=\'cpatl_import_category\']').val(item['value']);
        }
    });
    //--></script>

<?php } ?>