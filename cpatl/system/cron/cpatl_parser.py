#!/usr/bin/python2
# -*- coding: UTF-8 -*-
from urllib2 import urlopen
import json
import os, time
from datetime import datetime
import sys
import requests
import pandas as pd
######
start_time = time.time()
jsonRes = {
    'start_time': start_time,
    'countries': []
}

jsonResGlobal = {
    "success": 0
}
######


def getCurrencyConfig():
    gtableId = '14HTeIu5By3ZmOCsyYfB7-2WhaMuV2SrDXupxM6vry7M'
    gtableListId = '0'
    storageUrl = 'https://docs.google.com/spreadsheets/d/'+gtableId+'/export?format=csv&gid=' + gtableListId
    response = urlopen(storageUrl)
    cr = pd.read_csv( storageUrl )
    cr = cr.to_numpy()

    res = {}
    i = 0
    for row in cr:

        i +=1
        if i > 1 :
            res[ row[1] ] = row[3]

    return res

def sendTG(message):
    TOKEN = "5922330900:AAGEVENQE7glqWBo4RRd6lKedjC3iDHgMqQ"
    chat_id = "410092998"
    url = "https://api.telegram.org/bot"+TOKEN+"/sendMessage?chat_id="+chat_id+"&text="+message
    requests.get(url)

sendTG('start parsing CPA')
reload(sys)
sys.setdefaultencoding('utf-8')

os.environ['TZ'] = 'Europe/Moscow'
time.tzset()


import multiprocessing
import time



countryCurrencies = getCurrencyConfig()
def buildCurrency(geo):
    if geo not in countryCurrencies:
        if geo not in jsonRes['countries']:
            jsonRes['countries'].append(geo)
            sendTG( 'Добавьте страну '  + geo )
    else:
        return countryCurrencies[geo]


def parse():

    url = 'https://cpa.tl/api/offers'

    response = urlopen(url)
    data = response.read().decode('UTF-8')
    dataNew = json.loads(data)

    #with open( os.path.dirname(os.path.realpath(__file__))+'/data_test.json') as data_json:
    #    dataNew  = json.load(data_json)
    result = {}

    for offer in dataNew['data']:
        id = offer['id']
        if not result.has_key(id):
            result[id] = {}

        prices = {}
        titles = {}
        titles2 = {}
        geos = []


        for land in offer['landings']:
            titles[land['language_code']] = land['title']


        for goal in offer['goals']:

            currency = goal['landing_currency']
            if not currency:
                currency = buildCurrency( goal['geo'] )

            prices[ currency ] = {
                'price': goal['landing_price'],
                'geo': goal['geo'],
            }

            titles[goal['geo']] = goal['title']
            geos.append( goal['geo'] )




        result[ id ]['prices'] = prices
        result[ id ]['geos'] = geos
        result[ id ]['titles'] = titles
        result[ id ]['titles2'] = titles2

        result[ id ]['name'] = offer['title']
        result[ id ]['url']  = offer['url']
        result[ id ]['picture_url']  = offer['picture_url']
        result[ id ]['category']  = offer['category']
        result[ id ]['description']  = offer['description']


    jsonRes['time'] = time.strftime('%Y') + '-' +  time.strftime('%m') + '-' + time.strftime('%d') + ' ' + time.strftime('%H') + ':' + time.strftime('%M')
    jsonResGlobal['processed'] = jsonRes['processed'] = time.time() - jsonRes['start_time']
    jsonRes['offers'] = result
    jsonRes['offers_count'] = len(result)
    jsonResGlobal['offers_count'] = len(result)
    jsonResGlobal['time'] = jsonRes['time']
    jsonResGlobal['countries'] = jsonRes['countries']



    with open( os.path.join(   os.path.dirname(os.path.dirname(os.path.realpath(__file__))) , "storage/cpadata.json") , 'w') as f:
        json.dump( jsonRes, f, indent=4, ensure_ascii=False)


parse()

message = '[CPA] end script ' + str( time.time() - jsonRes['start_time'] )
sendTG(message)

jsonResGlobal['success'] = 1
json_object = json.dumps(jsonResGlobal, indent = 4)
sendTG(message + '\n' + json_object)
print(json_object)

