#!/usr/bin/python3

import pandas as pd
import csv
import os, time
from datetime import datetime
#from fuzzywuzzy import fuzz
from difflib import SequenceMatcher as SM
#import requests
import math
import urllib.request
import urllib.parse
import grequests

import json
import ssl

######
start_time = time.time()
jsonRes = {
    'start_time': start_time,
    'countries': []
}

jsonResGlobal = {
    "success": 0
}
######



def sendTG(message):
    TOKEN = "5922330900:AAGEVENQE7glqWBo4RRd6lKedjC3iDHgMqQ"
    chat_id = "410092998"
    url = 'https://api.telegram.org/bot'+TOKEN+'/sendMessage?'
    params = {'chat_id': chat_id, 'text': message}
    urllib.request.urlopen(url + urllib.parse.urlencode(params))

sendTG('start parsing COMBO')
#####
api_key = '536cb51422c735be406af24af9bb3909'
#####
import re

import sys

os.environ['TZ'] = 'Europe/Moscow'
time.tzset()






def getCurrencyConfig():
    gtableId = '14HTeIu5By3ZmOCsyYfB7-2WhaMuV2SrDXupxM6vry7M'
    gtableListId = '0'
    storageUrl = 'https://docs.google.com/spreadsheets/d/'+gtableId+'/export?format=csv&gid=' + gtableListId
    response = urllib.request.urlopen(storageUrl)
    cr = pd.read_csv( storageUrl )
    cr = cr.to_numpy()

    res = {}
    i = 0
    for row in cr:

        i +=1
        if i > 0 :
            res[ row[1] ] = row[3]

    return res



import multiprocessing
import time

removeArr = ['( LOW Price)', '(low price)', '(Low Price)', '( Full Price)', 'Full Price', 'Low Price']


SimilarFindedAll = {}
SimilarFindedCombo = {}
SimilarFindedCPA = {}


def clearString(strVal):
    orig = strVal
    strVal = strVal.lower()
    for strR in removeArr:
        strVal = strVal.replace(strR, '')
        strVal = strVal.replace(strR.lower(), '')

    return strVal




def checkSimilarFinal(id, name, shortName, prices, offers):
    similars = checkSimilar(id, name, shortName, prices, offers)

    SimilarFindedCombo[ id ] = similars

    for similar in similars:
        if similar['idCpa'] not in SimilarFindedCPA:
            SimilarFindedCPA[similar['idCpa']] = []
        SimilarFindedCPA[ similar['idCpa'] ].append(similar)



    return similars


def checkSimilar(id, name, shortName, prices, offers):
    name = clearString(name)
    similar = []
    #  ищем полные совпадения

    for key in offers:
        similarOffer = offers[key]
        name2 = similarOffer['name']
        name2Orig = similarOffer['name']

        name2 = clearString(name2)



        if(name2.strip() == name.strip() ):
            dataSimilar = {'id': key, 'name': name2Orig, 'percent': 1.0, 'idCpa': key, 'IdCombo': id, 'pricesCombo': prices, 'pricesCpa': similarOffer['prices'] }
            similar.append( dataSimilar )

    if( len(similar) > 0):
        return similar

    similarTest = []
    for key in offers:
        similarOffer = offers[key]
        name2 = similarOffer['name']



        if(shortName):
            percent = checkString(name2, shortName)
            if(percent > 0.6):

                similarTest.append( {'id': key, 'name': name2, 'percent': percent, 'idCpa': key, 'IdCombo': id, 'pricesCombo': prices, 'pricesCpa': similarOffer['prices'] } )


    if(len(similarTest) > 1):
        for item in similarTest:
            name2 = clearString( item['name'])

            percent = SM(None, name2, name).ratio()
            if(percent > 0.5):
                similar.append( item )
    else:
        similar = similarTest

    return similar

def checkString(s1, s2):

    s1 = clearString(s1)
    s2 = clearString(s2)

    ar1 = s1.strip().split(' ')
    ar2 = s2.strip().split(' ')
    result=list(set(ar1) & set(ar2))

    result = float(len(result))
    base = float(len(ar2))


    if( result > 0 ):
        return  ((result*100) / (base * 100))
    return 0



countryCurrencies = getCurrencyConfig()
def buildCurrency(cur, geo):

    if geo not in countryCurrencies:
        if geo not in jsonRes['countries']:
            jsonRes['countries'].append(geo)
            sendTG( 'Добавьте страну '  + geo )
    else:
        return countryCurrencies[geo]

def getAllOffers():
    res = []
    url = 'https://api.adcombo.com/offer/info/?api_key='+api_key+'&per_page=100&page=1'
    pages = 0
    with urllib.request.urlopen(url) as url:
        data = json.load(url)
        pages = math.ceil(data['total'] / data['per_page'])

    url = []

    #pages = 2
    for i in range(pages):
        url.append('https://api.adcombo.com/offer/info/?api_key='+api_key+'&per_page=100&page='+str(i+1))

    rs = (grequests.get(u) for u in url)
    for r in grequests.map(rs, size=8):
        if r.status_code == 200:
            dataNew = r.json()
            for offer in dataNew['offers']:
                res.append(offer)
        else:
            sendTG('')
    return res

def getAllOffersOld():
    page = 1
    isContinue = True
    res = []

    #with open( os.path.join(   os.path.dirname(os.path.dirname(os.path.realpath(__file__))) , "storage/combo-dump.json") ) as data_json:
    #    dataNew  = json.load(data_json)
    #    return dataNew

    while isContinue:
        url = 'https://api.adcombo.com/offer/info/?api_key='+api_key+'&per_page=100&page='+str(page)
        context = ssl._create_unverified_context()

        response = urllib.request.urlopen(url, context=context)
        dataNew = response.read()
        dataNew = json.loads(dataNew)
        isContinue = False # len(dataNew['offers']) == 100
        for offer in dataNew['offers']:
            res.append(offer)

        with open( os.path.join(   os.path.dirname(os.path.dirname(os.path.realpath(__file__))) , "storage/combo-dump.json") , 'w') as f:
            json.dump( res, f, indent=4, ensure_ascii=False)

        page += 1
        print(url)

    return res

def parse():

    dataNew = getAllOffers()


    # Для сравнения
    urlSimple = 'https://georgia-market.ru/system/storage/cpadata.json'
    context = ssl._create_unverified_context()

    responseSimple = urllib.request.urlopen(urlSimple, context=context)
    dataSimple = responseSimple.read().decode('UTF-8')
    dataSimpleArr = json.loads(dataSimple)

    result = {}
    reg = re.compile('[^a-zA-Z ]')

    i = 0
    for offer in dataNew:

        if(offer['state'] != 'active'):
            continue

        if(len(offer['total_price'].keys()) < 1):
            continue

        if len(offer['thumbs']) < 1:
            continue

        #if( len(offer['total_price'].keys()) > 1 ):
        #    print(offer)

        #continue

        i = i + 1
        id = offer['id']
        if id not in result:
            result[id] = {}

        prices = {}
        titles = {}
        titles2 = {}
        geos = []


       # for land in offer['landings']:
       #     titles[land['language_code']] = land['title']


        offerCountries = offer['countries'].split(',')
        allowedCountries = []
        itemprice = {}
        itempriceCurrency = {}

        if(len(offer['total_price'].keys()) == 1 and len(offerCountries) == 1):
            allowedCountries = offerCountries
            itemprice[ offerCountries[0] ] = offer['total_price']['default'][0]['price']
            itempriceCurrency[ offerCountries[0] ] = offer['total_price']['default'][0]['currency']
        else:
            schemaCountries = {}

            for country in offerCountries:
                currency = buildCurrency( '', country  )
                if (currency not in schemaCountries ):
                    schemaCountries[currency] = 1

            for country in offerCountries:
                if( country in offer['total_price'].keys() ):

                    itemprice[country] = offer['total_price'][country][0]['price']
                    itempriceCurrency[country] = offer['total_price'][country][0]['currency']
                    allowedCountries.append( country )
                else:

                    ### Анализируем валюты !!!!

                    if(len(schemaCountries.keys()) == 1):
                        itemprice[country] = offer['total_price']['default'][0]['price']
                        itempriceCurrency[country] = offer['total_price']['default'][0]['currency']
                    #else:
                        #sendMessage('ERROR')


       # exit()

        for goalGeo in itemprice:
            currency = buildCurrency( '', goalGeo  )
            if currency:
                price = str(itemprice[goalGeo])
                price = price.replace("[^\d\.]", '')
                prices[currency] = {
                    'price': price,
                    'currencyOrig': itempriceCurrency[goalGeo],
                    'geo': goalGeo,
                }

                geos.append( goalGeo )

        result[ id ]['prices'] = prices
        result[ id ]['geos'] = geos



        #result[ id ]['titles'] = titles
        #result[ id ]['titles2'] = titles2

        result[ id ]['name'] = offer['name']

        short = reg.sub('', offer['name'])
        short = re.sub(r'\s+', ' ', short)
        short = short.strip()
        result[ id ]['name_short'] = short
        result[ id ]['url']  = offer['partner_url']
        result[ id ]['picture_url']  = ''
        if len(offer['thumbs']) > 0:
            result[ id ]['picture_url'] = offer['thumbs'][0]


        result[ id ]['category']  = ' '.join(map(str, offer['categories']))


        ###
        desc = ''
        if( len(offer['description'].keys()) > 0 ):
            descKey = next(iter( offer['description'].keys()))
            desc = offer['description'][  descKey ]

        result[ id ]['description']  = desc
        result[ id ]['similar'] = checkSimilarFinal(id, offer['name'], short, prices, dataSimpleArr['offers'])

    jsonRes['time'] = time.strftime('%Y') + '-' +  time.strftime('%m') + '-' + time.strftime('%d') + ' ' + time.strftime('%H') + ':' + time.strftime('%M')
    jsonResGlobal['processed'] = jsonRes['processed'] = time.time() - jsonRes['start_time']
    jsonRes['offers'] = result
    jsonRes['offers_count'] = len(result)
    jsonResGlobal['offers_count'] = len(result)
    jsonResGlobal['time'] = jsonRes['time']
    jsonResGlobal['countries'] = jsonRes['countries']

    with open( os.path.join(   os.path.dirname(os.path.dirname(os.path.realpath(__file__))) , "storage/combo.json") , 'w') as f:
        json.dump( jsonRes, f, indent=4, ensure_ascii=False)

    with open( os.path.join(   os.path.dirname(os.path.dirname(os.path.realpath(__file__))) , "storage/similarCombo.json") , 'w') as f:
        json.dump( { 'combo': SimilarFindedCombo, 'cpa': SimilarFindedCPA }, f, indent=4, ensure_ascii=False)



parse()

message = '[Combo] end script ' + str( time.time() - start_time )
sendTG(message)

jsonResGlobal['success'] = 1
json_object = json.dumps(jsonResGlobal, indent = 4)
sendTG(message + '\n' + json_object)
print(json_object)



