<?php
ignore_user_abort(true);
set_time_limit(0);

////
////
///


$command = 'python3 ' . __DIR__ . '/combo_parser.py';
$command = escapeshellcmd($command);
$output = shell_exec($command);

$json = @json_decode($output, true);
if(is_array($json) && $json['success']) {
    echo $output;
    die();
}

echo json_encode(['success'=>0, 'msg'=>'Error system', 'response'=>$output, 'command'=>$command]);