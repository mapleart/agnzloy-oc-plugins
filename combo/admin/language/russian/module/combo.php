<?php

// Heading Goes here:
$_['heading_title']          = 'Интеграция с Combo';
$_['combo_title']            = 'Интеграция с Combo';

// Text
$_['text_module']                = 'Модули';
$_['text_success']               = 'Настройки успешно сохранены';
$_['text_notice']                = 'Внимание! Модель товара должены совпадать с id в выгрузке';
$_['combo_base_settings']    = 'Настройки соединения';
$_['combo_extended_settings']    = 'Расширенные настройки';

$_['combo_url']              = 'Адрес API';
$_['combo_apigeo']           = 'Гео зона для api';


// Errors
$_['error_permission']           = 'У вас недостаточно прав на изменение настроек модуля';

