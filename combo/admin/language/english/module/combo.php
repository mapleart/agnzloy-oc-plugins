<?php
// Heading Goes here:
$_['heading_title'] = 'Integration with Combo';
$_['combo_title'] = 'Integration with Combo';

// Text
$_['text_module'] = 'Modules';
$_['text_success'] = 'Settings saved successfully';
$_['text_notice'] = 'Attention! The product model in the  must match the id in the upload';
$_['combo_extended_settings'] = 'Advanced Settings';

$_['combo_url'] = 'Spatial address';
$_['combo_apigeo'] = 'Geo zone for api';


// Errors
$_['error_permission'] = 'You don\'t have enough rights to change the module settings';