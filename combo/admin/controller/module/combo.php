<?php
require_once DIR_SYSTEM.'MapleHelper.php';
require_once __DIR__.'/excel.php';
class ControllerModuleCombo extends Controller
{

    ///
    public $cronToken = '';
    public $tgToken = '';
    public $tgUsers = '';

    ////
    public $siteOffers = '';  // Урл сайта - где запущен крон
    public $urlFile = '';  // Урл файла после парсинга
    public $file_data = null;  // Урл файла после парсинга
    ////

    public $geos = []; // Сразу хранит все гео зоны
    public $replaceModel = '';  // То что нужно брать из модели товара чтобы модуль привел в вид сpa

    public $offers = 'init';   // Полный список офферов от сети
    public $excludedOfferIds = []; // Иды офферов которые исключить из выборки
    public $similarIgnoreOfferIds = []; // Иды офферов которые не нужно проверять на схожесть

    public $offersGeo = []; // все офферы которые подходяь для $geos
    public $offersGeoCreated = []; // Ид офферов которые созданы в магазине ( по аналогии с $productsUpdateIds)
    public $offersGeoNew = []; // новые товары в выбраной зоне, которых нет в магазине
    public $offersSimilar = []; // Товары которые похожи из друшой выгрузки

    public $offersDeleted = [];  // офферы которые есть в магазине но в выгрузке их нет в $geos


    public $productsUpdateIds = []; // Иды товаров которые есть в магазине и в выгрузке
    public $productsPriceOld  = []; // Товары у которых отличается цена




    public $lastTime = 0;
    public $liveTime = 0;

    public $notify_email = null;
    public $bNotifyNewOffers = false;
    public $bNotifyUpdate = false;
    public $bNotifyPriceNew = false; // Уведомлять об изменении цены ?
     public $status = false;

    public $currencies = [];
    public $currencySelected = '';


    public $categories = [];
    public $selectedCategories = [];
    public $modelPrefix = '';
    public $importCategory = 0;
    public $defQuantity = 9999;
    public $selectedStores = [];


    public function __construct($registry)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        parent::__construct($registry);
        $this->getModuleData();
    }

    private $_error = array();
    private $_warning = array();

    /**
     * Install method
     *
     * @return void
     */
    public function install()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('combo', array('combo_status' => 1));

        $this->load->model('extension/event');

        $this->model_extension_event
            ->addEvent(
                'combo',
                version_compare(VERSION, '2.2', '>=') ? 'catalog/model/checkout/order/addOrder/after' : 'post.order.add',
                'module/combo/order_create'
            );
    }

    /**
     * Create order on event
     *
     * @param int $order_id order identificator
     *
     * @return void
     */
    public function order_create($order_id)
    {
        $this->load->model('checkout/order');
        $this->load->model('account/order');

        $data = $this->model_checkout_order->getOrder($order_id);
        $data['products'] = $this->model_account_order->getOrderProducts($order_id);

        $this->log->write('create order');
        $this->log->write(print_r($data['products'] ,true));
    }

    /**
     * Uninstall method
     *
     * @return void
     */
    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('combo', array('combo_status' => 0));
        $this->model_extension_event->deleteEvent('combo');

    }

    /**
     * Главная страница
     *
     * @return void
     */
    public function index()
    {

        $this->load->model('setting/setting');
        $this->load->model('extension/module');
        $this->load->language('module/combo');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('/admin/view/stylesheet/combo.css');

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {


            $redirect = $this->url->link(
                'module/combo', 'token=' . $this->session->data['token'],
                'SSL'
            );


            if ($this->request->post && isset($this->request->post['form'])) {
                $form = $this->request->post['form'];
                if ($form == 'import'){
                    $this->import();

                    $this->response->redirect($redirect);

                }
            }


            $this->model_combo_combo->SaveCronJobs(isset($_POST['cron']) ? $_POST['cron'] : null);

            $this->model_setting_setting->editSetting(
                'combo',
                $this->request->post
            );

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($redirect);
        }

        $text_strings = array(
            'heading_title',
            'text_enabled',
            'text_disabled',
            'button_save',
            'button_cancel',
            'text_notice',
            'combo_title',
            'combo_url',
            'combo_apigeo',
            'combo_replace_model',
            'combo_base_settings',
            'combo_extended_settings',
        );

        $this->load->model('extension/extension');
        $_data = &$data;

        foreach ($text_strings as $text) {
            $_data[$text] = $this->language->get($text);
        }


        $_data['combo_errors'] = array();
        $_data['saved_settings'] = $this->model_setting_setting
            ->getSetting('combo');

        $config_data = array(
            'combo_status'
        );

        $_data['check_html'] = $this->checkSystemHtml();


        foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $_data[$conf] = $this->request->post[$conf];
            } else {
                $_data[$conf] = $this->config->get($conf);
            }
        }

        if (isset($this->error['warning'])) {
            $_data['error_warning'] = $this->error['warning'];
        } else {
            $_data['error_warning'] = '';
        }

        $_data['breadcrumbs'] = array();

        $_data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link(
                'common/home',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => false
        );

        $_data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link(
                'extension/module',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        $_data['breadcrumbs'][] = array(
            'text'      => $this->language->get('combo_title'),
            'href'      => $this->url->link(
                'module/combo',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        $_data['action'] = $this->url->link(
            'module/combo',
            'token=' . $this->session->data['token'], 'SSL'
        );

        $_data['cancel'] = $this->url->link(
            'extension/module',
            'token=' . $this->session->data['token'], 'SSL'
        );

        $_data['modules'] = array();

        if (isset($this->request->post['combo_module'])) {
            $_data['modules'] = $this->request->post['combo_module'];
        } elseif ($this->config->get('combo_module')) {
            $_data['modules'] = $this->config->get('combo_module');
        }

        $this->load->model('design/layout');
        $_data['token'] = $this->session->data['token'];
        $_data['layouts'] = $this->model_design_layout->getLayouts();
        $_data['header'] = $this->load->controller('common/header');
        $_data['column_left'] = $this->load->controller('common/column_left');
        $_data['footer'] = $this->load->controller('common/footer');

        if (isset($this->session->data['success'])) {
            $data['alert_notice'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['alert_notice'] = '';
        }
        if (isset($this->session->data['error'])) {
            $data['error_notice'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } else {
            $data['error_notice'] = '';
        }

        if($this->urlFile && count($this->geos) ) {

            $_data['parsed_file'] = [
                'live'=> $this->liveTime,
                'time'=> $this->lastTime,
                'count_offers'=> count($this->offers)
            ];

            $newOrdersHtml = $this->printNewOffers(true);
            $_data['offers_price_diff'] = $this->printOffersPriceDiff(true);
            $_data['new_offers']=$newOrdersHtml;
            $_data['currencies']=$this->currencies;
            $_data['categories']=$this->categories;
            $_data['selectedCategories']=$this->selectedCategories;
            $data['stores'] = $this->model_setting_store->getStores();
            $data['selectedStores'] = $this->selectedStores;


            $similarOrdersHtml = $this->printSimilarOffers(true);
            $_data['similar_offers']=$similarOrdersHtml;



            $this->load->model('catalog/category');

            $_data['combo_input_category']='';
            $_data['combo_import_category']=0;
            if($category_info = $this->model_catalog_category->getCategory( $this->importCategory ) ) {
                $_data['combo_input_category'] = $category_info['name'];
                $_data['combo_import_category']=$this->importCategory;
            }
        }



        $_data['cron_config']=$this->model_combo_combo->GetJobsTasks();

        ////
        ////
        ////
        $_data['countries']=MapleHelper::ExternalCountries();
        $_data['file_data']=$this->file_data;
        $_data['linkFileUpdate']=$this->siteOffers . 'system/cron/combo_parser.php';

        ////
        ////
        ////

        $_data['mainForm'] =$this->load->view('module/combo_main.tpl', $_data);
        $_data['notificationsForm'] =$this->load->view('module/combo_notifications.tpl', $_data);

        //$this->sync();

        $this->response->setOutput(
            $this->load->view('module/combo.tpl', $_data)
        );
    }

    public function remote_filesize ($url ) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $path_parts = pathinfo($url); //получаем название файла из URL
        $fp = fopen(__DIR__.'/'.$path_parts['basename'], 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);


        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
    }



    /**
     * Ипорт
     */
    public function uploadImage($sFileUrl, $sDirSave){


        if (!$aImageInfo = ($this->model_combo_combo->remoteGetImgSize($sFileUrl))) {
            return false;
        }


        /**
         * Копируем загруженный файл
         */
        if (!is_dir($sDirSave)) {
            @mkdir($sDirSave, 0777, true);
        }
        $path_parts = pathinfo($sFileUrl); //получаем название файла из URL

        if(isset($path_parts['extension'])) {
            $sExtension = $path_parts['extension'];
        } else {
            $sExtension = $path_parts['filename'];
        }


        $fileName = substr(md5(uniqid(mt_rand(), true)), 0, 10) . '.' . $sExtension;
        $sFileTmp = $sDirSave . $fileName;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sFileUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $fp = fopen($sFileTmp, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);


        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec( $ch );
        $http_status = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        $curl_errno = curl_errno( $ch );
        curl_close( $ch );

        // Get network stauts
        if ( $http_status != 200 ) {
            return false;
        }
        return $fileName;
    }

    public function makeSlug($title, $separator = '-')
    {
        $matrix = [
            'й' => 'i',    'ц' => 'c',  'у' => 'u',  'к' => 'k',    'е' => 'e',
            'н' => 'n',    'г' => 'g',  'ш' => 'sh', 'щ' => 'shch', 'з' => 'z',
            'х' => 'h',    'ъ' => '',   'ф' => 'f',  'ы' => 'y',    'в' => 'v',
            'а' => 'a',    'п' => 'p',  'р' => 'r',  'о' => 'o',    'л' => 'l',
            'д' => 'd',    'ж' => 'zh', 'э' => 'e',  'ё' => 'e',    'я' => 'ya',
            'ч' => 'ch',   'с' => 's',  'м' => 'm',  'и' => 'i',    'т' => 't',
            'ь' => '',     'б' => 'b',  'ю' => 'yu', 'ү' => 'u',    'қ' => 'k',
            'ғ' => 'g',    'ә' => 'e',  'ң' => 'n',  'ұ' => 'u',    'ө' => 'o',
            'Һ' => 'h',    'һ' => 'h',  'і' => 'i',  'ї' => 'ji',   'є' => 'je',
            'ґ' => 'g',    'Й' => 'I',  'Ц' => 'C',  'У' => 'U',    'Ұ' => 'U',
            'Ө' => 'O',    'К' => 'K',  'Е' => 'E',  'Н' => 'N',    'Г' => 'G',
            'Ш' => 'SH',   'Ә' => 'E',  'Ң '=> 'N',  'З' => 'Z',    'Х' => 'H',
            'Ъ' => '',     'Ф' => 'F',  'Ы' => 'Y',  'В' => 'V',    'А' => 'A',
            'П' => 'P',    'Р' => 'R',  'О' => 'O',  'Л' => 'L',    'Д' => 'D',
            'Ж' => 'ZH',   'Э' => 'E',  'Ё' => 'E',  'Я' => 'YA',   'Ч' => 'CH',
            'С' => 'S',    'М' => 'M',  'И' => 'I',  'Т' => 'T',    'Ь' => '',
            'Б' => 'B',    'Ю' => 'YU', 'Ү' => 'U',  'Қ' => 'K',    'Ғ' => 'G',
            'Щ' => 'SHCH', 'І' => 'I',  'Ї' => 'YI', 'Є' => 'YE',   'Ґ' => 'G',
        ];

        foreach ($matrix as $from => $to)  {
            $title = mb_eregi_replace($from, $to, $title);
        }

        $pattern = '![^'.preg_quote($separator).'\pL\pN\s]+!u';
        $title = preg_replace($pattern, '', mb_strtolower($title));

        $flip = $separator == '-' ? '_' : '-';

        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);

        return trim($title, $separator);
    }


    public function getProductUrlFromStore($productID){
        $this->load->model('setting/store');
        $this->load->model('catalog/product');

        if (version_compare(VERSION, '2.2.0', '>=')) {
            $_url = new Url($this->config->get('config_secure') ? HTTP_CATALOG : HTTPS_CATALOG);
            $_url->setDomain(HTTP_CATALOG);
        } else {
            $_url = new Url(HTTP_CATALOG, $this->config->get('config_secure') ? HTTP_CATALOG : HTTPS_CATALOG);
        }
        if ($this->config->get('config_seo_url')) {
            if (version_compare(VERSION, '2.2.0', '>=')) {
                if (class_exists('VQMod')) {
                    require_once(VQMod::modCheck((DIR_APPLICATION . '../catalog/controller/startup/seo_url.php')));
                } else {
                    // TODO: try loading OCMODed file
                    require_once(DIR_APPLICATION . '../catalog/controller/startup/seo_url.php');
                }
                $seo_url = new ControllerStartupSeoUrl($this->registry);
            } else {
                // Require the SEO class directly from relative path to /admin/index.php
                if (class_exists('VQMod')) {
                    require_once(VQMod::modCheck((DIR_APPLICATION . '../catalog/controller/common/seo_url.php')));
                } else {
                    require_once(DIR_APPLICATION . '../catalog/controller/common/seo_url.php');
                }
                $seo_url = new ControllerCommonSeoUrl($this->registry);
            }
        }

        $data['stores'][0] = array(
            'name'  => $this->config->get('config_name'),
            'url'   => HTTP_CATALOG,
            'ssl'   => HTTPS_CATALOG,
            'make'  => $_url
        );
        $stores = $this->model_setting_store->getStores();

        foreach ($stores as $store) {
            $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store['store_id'] . "' AND `key` = 'config_secure'");
            $secure = (int)$query->row['value'];

            if (version_compare(VERSION, '2.2.0', '>=')) {
                $__url = new Url($secure ? $store['url'] : $store['ssl']);
                $__url->setDomain($store['url']);
            } else {
                $__url = new Url($store['url'], $secure ? $store['url'] : $store['ssl']);
            }

            $data['stores'][$store['store_id']] = array(
                'name'  => $store['name'],
                'url'   => $store['url'],
                'ssl'   => $store['ssl'],
                'make'  => $__url
            );

            if ($this->config->get('config_seo_url')) {
                $data['stores'][$store['store_id']]['make']->addRewrite($seo_url);
            }
        }

        $product_stores = $this->model_catalog_product->getProductStores($productID);
        $row = [];

        foreach ($product_stores as $store) {
            if (!in_array($store, array_keys($data['stores'])))
                continue;
            $row[] = array(
                'name' => $data['stores'][$store]['name'],
                'href' => $data['stores'][$store]['make']->link('product/product&product_id=' . $productID)
            );
        }
       return $row;
    }

    public function import($bCron=false){
        $this->load->model('catalog/product');
        $this->load->model('localisation/language');

        if(!$this->currencySelected) {
            $this->session->data['error'] = 'Не указана валюта';
            return false;
        }

        if(!$this->importCategory) {
            $this->session->data['error'] = 'Не выбрана категория для импорта';
            return false;
        }


        if(!$this->replaceModel) {
            $this->session->data['error'] = 'Не указан префикс для модели';
            return false;
        }

        if(!count( $this->selectedStores )) {
            $this->session->data['error'] = 'Выберите магазины для загрузки';
            return false;
        }


        $countSkipCategory = 0;
        $countSkipImages = 0;
        $countSuccess = 0;
        $countError = 0;

        $result = [];

        foreach ($this->offersGeoNew as $offerId=>$newOffer) {
            $sDirSave = DIR_IMAGE . 'catalog/combo/';

            $priceSpecial = isset($newOffer['prices'][$this->currencySelected]) ? $newOffer['prices'][$this->currencySelected]['price'] : 0;
            $price = $priceSpecial * 2;


            if( $priceSpecial <= 0) {
                $priceSpecial = 1;
                $price = 99;
            }



            if(in_array($newOffer['category'], $this->selectedCategories)) {
                ++$countSkipCategory;
                continue;
            }


            // Скачиваем картинку, без нее не продалжать !
            if(!$imagePath = $this->uploadImage($newOffer['picture_url'], $sDirSave)) {
                ++$countSkipImages;
                #print_r($newOffer['picture_url']);
                #die();
                continue;
            }



            $imagePath = 'catalog/combo/' . $imagePath;

            $data = [];
            $data['name'] = $newOffer['name'];
            $data['model'] = $this->replaceModel . $offerId;
            $data['sku'] = '';
            $data['upc'] = '';
            $data['ean'] = '';
            $data['jan'] = '';
            $data['isbn'] = '';
            $data['mpn'] = '';
            $data['location'] = '';


            $this->load->model('setting/store');
            $data['product_store'] = $this->selectedStores;
            $data['keyword'] = $this->makeSlug($newOffer['name']).'-'.$offerId;
            $data['shipping'] = 1;
            $data['price'] = $price;
            $data['price_scecia'] = $priceSpecial;




            $this->load->model('catalog/recurring');

            $this->load->model('tool/image');

            $this->load->model('localisation/tax_class');

            $data['product_recurrings'] = array(); // Периодичность

            $data['tax_class_id'] = 0;

            $data['date_available'] = date('Y-m-d');
            $data['quantity'] = $this->defQuantity;
            $data['minimum'] = 1;
            $data['subtract'] = 1;
            $data['sort_order'] = 1;
            $data['stock_status_id'] = 0;
            $data['status'] = true;
            $data['weight'] = '';
            $data['weight_class_id'] = $this->config->get('config_weight_class_id');
            $data['length'] = '';
            $data['width'] = '';
            $data['height'] = '';
            $data['length_class_id'] = $this->config->get('config_length_class_id');
            $data['manufacturer_id'] = 0;


            // Categories
            $this->load->model('catalog/category');

            if($category_info = $this->model_catalog_category->getCategory($this->importCategory)) {
                $data['product_category']= [ $this->importCategory ];
            }



            $data['product_filters'] = array();// Filters
            $data['product_attributes'] = array();  // Attributes
            $data['product_options'] = array();


            $data['product_discounts'] = array();

            $data['product_special'][] = array(
                'customer_group_id' => 1,
                'priority'          => 0,
                'price'             => $priceSpecial,
                'date_start'        => '',
                'date_end'          => ''
            );

            $data['image'] = $imagePath;
            $data['product_images'][] = array(
                'image'      => $imagePath,
                'thumb'      => $this->model_tool_image->resize($imagePath, 100, 100),
                'sort_order' => 1
            );
            $data['product_downloads'] = array();
            $data['product_relateds'] = array();
            $data['product_reward'] = array();
            $data['product_layout'] = array();
            $data['points'] = array();




            $data['product_description'] = array();
            foreach ( $this->model_localisation_language->getLanguages() as $language) {
                $data['product_description'][ $language['language_id'] ] = array(
                    'name'=>$newOffer['name'],
                    'description'=>'',
                    'meta_description'=>'',
                    'meta_keyword'=>'',
                    'tag'=>'',
                    'meta_title'=>$newOffer['name'],
                );
            }


            if($productId=$this->model_catalog_product->addProduct($data)) {
                $result[$productId] = $data;
                ++$countSuccess;
            } else {
                ++$countError;
            }
        }

        $allSkip = $countSkipCategory + $countSkipImages;

        $table = [
            ['ID товара в Opencart', 'Название товара', 'Ссылка на товар', 'New', 'Цена', 'In stock', 'Ссылка на изображение товара', 'trafficlight']
        ];

        foreach ($result as $productId=>$offerData) {


            $urls = [];
            foreach ($this->getProductUrlFromStore($productId) as $storePath) {
                $urls[]=$storePath['href'];
            }

            $url = '';
            if(isset($urls[0])) {
                $url = $urls[0];
            }
            $name = $offerData['name'];
            $table[] = [$productId, $name, $url, 'New', $offerData['price_scecia'], 'In stock', HTTPS_CATALOG.'image/'.$offerData['image'], $offerData['model']];
        }

        if (file_exists(DIR_UPLOAD . 'combo.xlsx')) {
            unlink(DIR_UPLOAD . 'combo.xlsx');
        }

        $xlsx = Shuchkin\SimpleXLSXGen::fromArray( $table );
        $xlsx->saveAs(DIR_UPLOAD . 'combo.xlsx');

        //
        $htmlSuccess = "
            <h4>Ипорт новых товаров завершен</h4>
            <p>Успешно добавлено: $countSuccess</p>
            <p>Ошибки: $countError</p>
            <p>Пропущено: $allSkip (из них у {$countSkipImages} нет картинки и $countSkipCategory пропущены изза категории)</p>
        ";
        if($countSuccess > 0  || $countError > 0) {
            if($bCron) {
                print_r($htmlSuccess);
            }
            $this->sendMail('Отчет о новых товарах ' .implode(',', $this->geos), $htmlSuccess, DIR_UPLOAD . 'combo.xlsx');
        }



        $this->session->data['success'] = $htmlSuccess;

    }
    /**
     * Export orders
     */
    public function sync() {

        $this->load->model('setting/setting');
        $this->load->model('extension/extension');
        if(!$this->status) return;

        $token = isset($_GET['token']) ? $_GET['token'] : '';
        if($token !== $this->cronToken) {
            echo 'Ошибка доступа';
            die();
        }
        $keyGeo = implode(', ', $this->geos);


        if( !$this->urlFile || !count($this->geos) ) {
            echo 'Ошибка настроек '.PHP_EOL;
            return false;
        }



        $countOffers  = count($this->offers);
        $countGeoOffers  = count($this->offersGeo);
        $countToUpdate  = count($this->productsUpdateIds);

        $notFinded = $this->offersDeleted;


        $allProducts = $this->model_combo_combo->getProducts($this->replaceModel);



        $res = [
            'countUpdatePrice'=>0,
            'countNew'=>0,
            'countActive'=>0,
        ];


        $bCron = true;
        ///
        /// Обновить цены
        ///
        if($this->bNotifyPriceNew) {
            $countOffers = count($this->offers);

            $newOrdersHtml = $this->printOffersPriceDiff(true, true, true);
            $oldOrdersCount = count($this->productsPriceOld);

            if ($this->bNotifyPriceNew && $oldOrdersCount > 0) {


                $html = "<h2>Инфо о выгрузке {$keyGeo}</h2>
<p>Количество офферов {$countOffers}</p>
<p>Время жизни файла парсинга {$this->liveTime}</p>
            ";

                if($this->tgUsers) {
                    MapleHelper::sendTelegramMessage($html . PHP_EOL. PHP_EOL . HTTPS_CATALOG, 'Изменение цен ' . $keyGeo . '[COMBO]', $this->tgUsers, $this->tgToken);
                }


                if($this->notify_email ) {
                    $html = "
<h2> Изменение цен </h2>
 {$newOrdersHtml}";


                    $this->sendMail('Изменение цен ' . $keyGeo, $html);

                }


            }
        }



        // Выкл все найденые товары
        $this->model_combo_combo->hideAllProducts($this->replaceModel, $allProducts);
        $this->model_combo_combo->process($this->productsUpdateIds);


        $res['countActive'] = count($this->productsUpdateIds);

        ///
        /// Импортирует
        ///
        $countNew = $this->import(true);
        $res['countNew'] = $countNew; // Сохраним в отчет



        if( $this->bNotifyUpdate ) {
            $newOrdersHtml = $this->printNewOffers(false);

            $html = "<h2>Инфо о выгрузке {$keyGeo}</h2>
<p>Количество офферов {$countOffers}</p>
<p>Время жизни файла парсинга {$this->liveTime}</p>
<h2>Инфо о результатах</h2>
<p>Всего офферов для гео-зоны {$countGeoOffers}</p>
<p>Товары которые подходят для зоны $keyGeo: {$countToUpdate}</p>

<h2>Инфо о товарах которые есть в магазине но нет в выгрузке (будут скрыты)</h2>
<p>" . implode(', ', $notFinded) . "</p>";


            if($this->tgUsers) {
                MapleHelper::sendTelegramMessage($html . PHP_EOL. PHP_EOL . HTTPS_CATALOG, 'Отчет об обновлении ' . $keyGeo . '[COMBO]', $this->tgUsers, $this->tgToken);
            }

            if($this->notify_email) {
                $html = "<h2> Новые товары </h2>
{$newOrdersHtml}";
                if($countNew) {
                    $this->sendMail('Отчет об обновлении' . $keyGeo, $html);
                }
            }

        }



        MapleHelper::sendDebug(json_encode($res), '[ COMBO '.HTTPS_CATALOG.'] Отчет о выгрузке ');
        echo json_encode($res);
    }

    public function newOffersCron(){
        $token = isset($_GET['token']) ? $_GET['token'] : '';
        if($token !== $this->cronToken) {
            echo 'Ошибка доступа';
            die();
        }

        $this->newOffers(true);
    }

    public function newOffers($bCron=false) {
        $this->load->model('setting/setting');
        $this->load->model('extension/extension');

        $keyGeo = implode(',', $this->geos);
        if(!$this->status) return;
        if( !$this->urlFile || ! count($this->geos) ) {
            echo 'Ошибка настроек '.PHP_EOL;
            return false;
        }

        if($bCron) {
            var_dump($this->bNotifyPriceNew);

        }


        if($this->bNotifyNewOffers) {

            $countOffers  = count($this->offers);

            $newOrdersHtml =  $this->printNewOffers(true);
            $newOrdersCount = count($this->offersGeoNew);

            if( $this->bNotifyNewOffers && $this->notify_email && $newOrdersCount > 0) {

                $html = "<h2>Инфо о выгрузке {$keyGeo}</h2>
<p>Количество офферов {$countOffers}</p>
<p>Время жизни файла парсинга {$this->liveTime}</p>";

                MapleHelper::sendTelegramMessage($html. PHP_EOL. PHP_EOL . HTTPS_CATALOG, 'Новые товары в выгрузке ' . $keyGeo. ' [COMBO]', $this->tgUsers, $this->tgToken);

                $html = "<br>
<h2> Новые товары </h2>
{$newOrdersHtml}";

                $this->sendMail('Новые товары в выгрузке ' . $keyGeo, $html);
            }
        }
    }


    /**
     * Создание переменных для модуля
     * @return false
     */
    public function getModuleData() {
        $this->load->model('combo/combo');
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('combo');

        $this->status = isset($settings['combo_status']) ? $settings['combo_status'] : null;


        $this->siteOffers = MapleHelper::ExternalStorageGet('combo_offers_file');
        $this->urlFile = $this->siteOffers . 'system/storage/combo.json';

        $keyGeo = isset($settings['combo_apigeo']) ? $settings['combo_apigeo'] : null;
        foreach (explode(',', $keyGeo) as $geoStr){
            $this->geos[] = trim(strtoupper(str_replace(' ', '', $geoStr)));
        }

        $this->replaceModel = isset($settings['combo_replace_model']) ? $settings['combo_replace_model'] : null;
        $this->notify_email = isset($settings['combo_notify_email']) ? $settings['combo_notify_email'] : null;
        $this->cronToken = MapleHelper::ExternalStorageGet('cron_web_token', 'fb243070a8b2bada4779fd2ddc7686b03377610b');
        $this->tgToken = isset($settings['combo_tg_token']) ? $settings['combo_tg_token'] : '';
        $this->tgUsers = isset($settings['combo_tg_users']) ? $settings['combo_tg_users'] : '';

        $this->bNotifyNewOffers = isset($settings['combo_notify_new_offers']) ? $settings['combo_notify_new_offers'] : false;
        $this->bNotifyUpdate = isset($settings['combo_notify_update_process']) ? $settings['combo_notify_update_process'] : false;
        $this->bNotifyPriceNew = isset($settings['combo_notify_price_new']) ? $settings['combo_notify_price_new'] : false;



        $countries = MapleHelper::ExternalCountries();
        $this->currencySelected = '';

        if(isset($countries[$keyGeo])) {
            $this->currencySelected = $countries[$keyGeo]['currency'];
        }

        $this->selectedCategories  = isset($settings['combo_categories']) ? $settings['combo_categories'] : [];
        $this->selectedStores  = isset($settings['combo_store']) ? $settings['combo_store'] : [];
        $this->importCategory  = isset($settings['combo_import_category']) ? $settings['combo_import_category'] : 0;
        $this->defQuantity  = isset($settings['combo_quantity']) ? (int)$settings['combo_quantity'] : 9999;

        if(!$this->urlFile) {
            return;
        }

        if($this->offers == 'init') {
            try {
                $json = file_get_contents($this->urlFile);
                $json = json_decode($json, true);

                $this->lastTime = $json['time'];

            } catch (Exception $exception) {
                echo 'Ошибка! Не найден файл парсинга. Пункт 1.1'.PHP_EOL;
                return false;
            }

            $this->file_data = [
                'offers_count'=>$json['offers_count'],
                'countries'=>$json['countries'],
                'processed'=>(int)$json['processed'],
                'time'=>(int)$json['time'],
            ];


            date_default_timezone_set("Europe/Moscow");

            $timeCreate = strtotime($json['time']);
            $currentTime = time();
            $this->liveTime = ($currentTime - $timeCreate) / 60;
            //

            $this->offers = [];
            foreach ($json['offers'] as $offerId => $offer) {
                $offerNew = $offer;
                foreach ($offerNew['prices'] as $key=>$price) {
                    $offerNew['prices'][$key]['price'] = ceil( (float)$price['price']);
                }
                $this->offers[$offerId] = $offerNew;
            }
        }


        $this->load->model('combo/combo');

        $allProducts = $this->model_combo_combo->getProducts($this->replaceModel);

        $comboIds = [];
        foreach ($allProducts as $productData) {
            $IdCombo = str_replace($this->replaceModel, '', $productData['model']);
            $comboIds[] = $IdCombo;

            if( isset($this->offers[$IdCombo]) ) {
                $bSuccess = false;
                foreach ($this->offers[$IdCombo]['geos'] as $code) {
                    if( in_array($code, $this->geos) ) {
                        $bSuccess = true;
                    }
                }

               if( $this->currencySelected && isset($this->offers[$IdCombo]['prices'][$this->currencySelected])){
                   // Если выбрана валюта,  высчитываем несоответствия цен
                   $productPrice = (float)$productData['special'];
                   //$offerPrice =  (float)$this->offers[$IdCombo]['prices'][$this->currencySelected]['price'];

                   $priceSpecial = isset($this->offers[$IdCombo]['prices'][$this->currencySelected]['price']) ? (float)$this->offers[$IdCombo]['prices'][$this->currencySelected]['price'] : 0;
                   $price = $priceSpecial * 2;


                   if( $priceSpecial <= 0) {
                       $priceSpecial = 1;
                       $price = 99;
                   }



                   if($priceSpecial != $productPrice) {

                       $data = [
                           'id'=>$productData['product_id'],
                           'picture_url'=>$this->offers[$IdCombo]['picture_url'],
                           'category'=>$this->offers[$IdCombo]['category'],
                           'name'=>$this->offers[$IdCombo]['name'],
                           'urlCpa'=>$this->offers[$IdCombo]['url'],
                           'IdCombo'=>$IdCombo,
                           'price'=>$productPrice,
                           'price_new'=>$priceSpecial,
                           'edit' => $this->url->link('catalog/product/edit', 'token=' . (isset( $this->session->data['token']) ? $this->session->data['token'] : '') . '&product_id=' . $productData['product_id'], 'SSL')
                       ];

                       $this->productsPriceOld[ $productData['product_id'] ] = $data;
                   }

               }

                if($bSuccess) {
                   $this->offersGeoCreated[] = $IdCombo;
                   $this->productsUpdateIds[] = $productData['product_id'];
                } else {
                    $this->offersDeleted[] = $IdCombo;
                }

            }
        }

        $this->offersDeleted = array_unique($this->offersDeleted);

        $exceptions = isset($settings['combo_exceptions']) ? $settings['combo_exceptions'] : null;

        $this->excludedOfferIds = [];
        foreach (explode(',', $exceptions) as $exeption){
            if(!$exeption) continue;
            $this->excludedOfferIds[] = trim(strtoupper(str_replace(' ', '', $exeption)));
        }


        $currencies = [];
        $categories = [];
        $settingsCpatl = $this->model_setting_setting->getSetting('cpatl');
        $replaceModelCpatl = isset($settingsCpatl['cpatl_replace_model']) ? $settingsCpatl['cpatl_replace_model'] : null;

        $similarIgnoreIds = isset($settings['combo_similar_ignore']) ? $settings['combo_similar_ignore'] : null;

        $this->similarIgnoreOfferIds = [];
        foreach (explode(',', $similarIgnoreIds) as $ignoreId){
            if(!$ignoreId) continue;
            $this->similarIgnoreOfferIds[] = trim(strtoupper(str_replace(' ', '', $ignoreId)));
        }


        foreach ($this->offers as $offerID=>$offerData) {


            $categories[ $offerData['category'] ] = 1;

            $bSuccess = false;
            $bSimilar = false;
            foreach ($offerData['geos'] as $code) {
                if( in_array($code, $this->geos) ) {
                    $this->offersGeo[] = $offerData;
                    $bSuccess = true;
                } else {

                }
            }

            //
            if( $settingsCpatl && $bSuccess && count($offerData['similar']) && !in_array($offerID, $this->similarIgnoreOfferIds)   ) {

                $findedProduct = false;
                $similarResult = [];

                $priceBase = isset($offerData['prices'][$this->currencySelected]) ? $offerData['prices'][$this->currencySelected]['price'] : 0;



                foreach ($offerData['similar'] as $similarItem) {

                    $productCpatl = $this->model_combo_combo->getProduct( $replaceModelCpatl . $similarItem['id']);

                    $priceSimilar = isset($similarItem['pricesCpa'][$this->currencySelected]) ? $similarItem['pricesCpa'][$this->currencySelected]['price'] : 0;


                   //
                    //
                    //
                    if($priceSimilar == $priceBase) {
                        if( isset($productCpatl['product_id']) ){
                            $findedProduct = true;
                            $productCpatl['edit'] = $this->url->link('catalog/product/edit', 'token=' . (isset( $this->session->data['token']) ? $this->session->data['token'] : '') . '&product_id=' . $productCpatl['product_id'], 'SSL');
                        }

                        $similarItem['product'] = $productCpatl;

                        $similarResult[]=$similarItem;
                    }


                    $bSimilar = $findedProduct;

                }


                $offerData['similar'] = $similarResult;
                if($bSimilar) {
                    $this->offersSimilar[$offerID] = $offerData;
                }
            }
            foreach ($offerData['prices'] as $currency=>$priceData) {
                $currencies[$currency] = 1;
            }

            if(!$bSimilar && $bSuccess && !in_array($offerID, $comboIds) && !in_array($offerID, $this->excludedOfferIds)) {
                $this->offersGeoNew[$offerID]=$offerData;
            }


        }

        $currencies = array_keys($currencies);
        sort($currencies);
        $this->currencies =$currencies;


        $categories = array_keys($categories);
        sort($categories);
        $this->categories = $categories;
        if($this->liveTime > 60) {
            $this->_warning['live_time'] = 'Внимание! Файл парсинга давно не обновлялся, проверьте крон. Пункт 1.1';
        }

    }

    public function getNewOffers() {

    }


    public function printSimilarOffers($bPhoto = true) {

        $result = $this->offersSimilar;
        $table = '<table class="table"><thead><tr>  <th>ID</th>   ' . ($bPhoto ? '<th>Фото</th>' : '')  . ' <th>Название</th>  <th> Цена</th> <th>Товары на сайте</th> </tr></thead> <tbody>';

        foreach ($result as $offerId=>$offer) {
            $offerName = $offer['name'];
            $offerCategory = $offer['category'];
            $price = isset($offer['prices'][$this->currencySelected]) ? $offer['prices'][$this->currencySelected]['price'] : 0;
            //$offerLink = '<a href="'.$offer['url'].'" target="_blank">'.$offer['url'].'</a>';
            $htmlSimilar = '';
            foreach ($offer['similar'] as $similar){
                $productHtml = '';

                if($product = $similar['product']) {
                    $productHtml .= ' - <a href="'.$product['edit'].'">редактировать [ Цена '.$product['special'].']</a><br>';
                }
                $htmlSimilar .= '<p>'. $similar['name']. $productHtml .'</p>';
            }
            
            $offerNImg = $bPhoto ? '<td><img src="'.$offer['picture_url'].'" style="width: 80px;"></td>' : '';
            $table .= "<tr> <td>{$offerId}</td> {$offerNImg}  <td>{$offerName}</td> <td>{$price}</td> <td>$htmlSimilar</td> </tr>";

        }
        $table .= '</tbody></table>';

        return $table;

    }


    public function printNewOffers($bPhoto = true) {

        $result = $this->offersGeoNew;
        $table = '<table class="table"><thead><tr>  <th>ID</th>   ' . ($bPhoto ? '<th>Фото</th>' : '')  . ' <th>Название</th>  <th> Цена</th> </tr></thead> <tbody>';

        foreach ($result as $offerId=>$offer) {
            $offerName = $offer['name'];
            // $offerCategory = $offer['category'];
            $price = isset($offer['prices'][$this->currencySelected]) ? $offer['prices'][$this->currencySelected]['price'] : 0;
            // $offerLink = '<a href="'.$offer['url'].'" target="_blank">'.$offer['url'].'</a>';
            $offerNImg = $bPhoto ? '<td><img src="'.$offer['picture_url'].'" style="width: 80px;"></td>' : '';
            $table .= "<tr> <td>{$offerId}</td> {$offerNImg}  <td>{$offerName}</td> <td>{$price}</td>  </tr>";

        }
        $table .= '</tbody></table>';

        return $table;

    }

    public function printOffersPriceDiff($bPhoto = true, $bCron=false, $bAutoupdate=false) {

        $result = $this->productsPriceOld;
        $table = '<table class="table"><thead><tr>  <th>Geo</th> <th>ID</th>   ' . ($bPhoto ? '<th>Фото</th>' : '')  . ' <th>Название</th> <th>Цена</th> <th>Новая цена</th> <th>Ссылка</th> </tr></thead> <tbody>';

        $geo = implode(',', $this->geos);
        foreach ($result as $pID=>$offer) {
            $offerId = $offer['IdCombo'];
            $offerName = $offer['name'];
            $offerCategory = $offer['category'];
            $price = $offer['price'] . ' ' . $this->currencySelected;
            $priceNew = $offer['price_new']. ' ' . $this->currencySelected;
            $offerLink = '<a href="'.$offer['edit'].'" target="_blank">редактировать</a>';
            $offerNImg = $bPhoto ? '<td><img src="'.$offer['picture_url'].'" style="width: 80px;"></td>' : '';
            $table .= "<tr> <td>{$geo}</td> <td>{$offerId}</td> {$offerNImg}  <td>{$offerName}</td>  <td>{$price}</td>  <td>{$priceNew}</td>  <td>{$offerLink}</td></tr>";

            if($bAutoupdate && $bCron) {
                $priceSpecial = $priceNew;
                $price = $priceSpecial * 2;


                if( $priceSpecial <= 0) {
                    $priceSpecial = 1;
                    $price = 99;
                }

                // updatePrice

                $this->load->model('combo/combo');
                $this->model_combo_combo->updatePrice($pID, $price, $priceSpecial );
            }
        }
        $table .= '</tbody></table>';

        return $table;
    }

    /**
     * Validate
     *
     * @return bool
     */
    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/combo')) {
            $this->_error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->_error) {
            return true;
        } else {
            return false;
        }
    }



    private function sendMail( $subject, $html, $file = '')
    {
        /*$mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout'); */

        $mail = new Mail();
        $mail->protocol = 'smtp';
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = 'ssl://smtp.beget.com';
        $mail->smtp_username = 'robot@kazahstan-online-market.ru';
        $mail->smtp_password = 'VO&gfl9N';
        $mail->smtp_port = 465;
        $mail->smtp_timeout = 10;
        //$mail->setTo('mapleart@yandex.ru');

        $mail->setFrom('robot@kazahstan-online-market.ru');
        //  $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject('[combo] '.$subject);
        $mail->setTo($this->notify_email);

        // $mail->setTo($this->notify_email);
        // $mail->setTo('alex.maple@icloud.com');
        //$mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        //  $mail->setSubject('[cpa.tl] '.$subject);



        if($file) {
            $mail->addAttachment($file);   // I took this from the phpmailer example on github but I'm not sure if I have it right.
        }

        $mail->setHtml($html  .'<br>' . $this->checkSystemHtml() );
        $mail->send();
    }

    public function checkSystemHtml(){
        $data = $this->checkSystem();

        $aErrors = $data['errors'];
        $aWarnings = $data['warnings'];



        $htmlErrors =  '';
        if(count($aErrors)) {
            $htmlErrors .= '<div style="color: #ecf0f1; background: #e74c3c; padding: 15px; border-radius: 4px; margin-bottom: 20px;">';
            foreach ($aErrors as $error) {
                $htmlErrors .= '<p style="margin: 0; padding: 5px 0;"> ' . $error['text'] . '</p>';
            }
            $htmlErrors .= '</div>';
        }

        $htmlWarnings = '';
        if(count($aWarnings)) {
            $htmlWarnings .= '<div style="color: #ecf0f1; background: #e67e22; padding: 15px; border-radius: 4px; margin-bottom: 20px;">';
            foreach ($aWarnings as $warning) {
                $htmlWarnings .= '<p style="margin: 0; padding: 5px 0;"> ' . $warning['text'] . '</p>';
            }
            $htmlWarnings .= '</div>';
        }

        $htmlSuccess = '';
        if(!count($aErrors)) {
            $htmlSuccess = '<div style="color: #ecf0f1; background: #27ae60; padding: 15px; border-radius: 4px; margin-bottom: 20px; font-weight: bold;">Проблем в работе модуля не обнаружено</div>';
        }

        return $htmlErrors.$htmlWarnings.$htmlSuccess;
    }
    public function checkSystem(){
        $aErrors = [];
        $aWarnings = [];

        $this->getModuleData();

        if(!$this->status) {
            $aErrors[]=['text'=>'Плагин не активирован'];
        }

        if(!$this->urlFile) {
            $aErrors[]=['text'=>'Файл с офферами не найден'];
        }

        // UZ

        if(!count($this->geos)) {
            $aErrors[]=['text'=>'Не указаны гео зоны!'];
        }

        if(!$this->replaceModel) {
            $aErrors[]=['text'=>'Не указана часть модели для поиска офферов'];
        }

        if(!$this->notify_email) {

            if( $mail = $this->config->get('config_email') ) {
                $aErrors[]=['text'=>'Не указан email для отправки уведомлений, будет отправлено на ' . $mail];
            } else {
                $aErrors[]=['text'=>'Не указан email для отправки уведомлений'];
            }
        }

        if(!$this->currencySelected) {
            $aErrors[]=['text'=>'Не указана валюта, импорт товаров не возможен'];
        }


        if(!$this->importCategory) {
            $aErrors[]=['text'=>'Не указана категория'];
        }

        if(!$this->offers || ($this->offers && is_array($this->offers) && !count($this->offers))){
            $aErrors[]=['text'=>'Файл с офферами не доступен или пустой'];
        }

        if($this->liveTime > 10) {
            $aWarnings['live_time'] = ['text'=>'Внимание! Файл парсинга давно не обновлялся, проверьте крон. Пункт 1.1'];
        }

        $this->load->model('combo/combo');
        $event = $this->model_combo_combo->checkEvent(  'combo',
            version_compare(VERSION, '2.2', '>=') ? 'catalog/model/checkout/order/addOrder/after' : 'post.order.add',
            'module/combo/order_create' );

        if(!$event) {
            $aErrors[]=['text'=>'Плагин не обновлен'];
        }

        return [
            'errors'=>$aErrors,
            'warnings'=>$aWarnings
        ];
    }
}
