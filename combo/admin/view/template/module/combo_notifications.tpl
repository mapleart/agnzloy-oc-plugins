<h3>Уведомления и CRON</h3>

<div class="alert alert-info">
    Cron работает автоматически через сервис <a href="https://cron-job.org">cron-job.org</a>
</div>


<div class="form-group">
    <label for="combo_cron_enabled">Включить cron ?</label><br>

    <select id="combo_cron_enabled" name="combo_cron_enabled" class="form-control">
        <option value="1" <?php if (isset($saved_settings['combo_cron_enabled']) && $saved_settings['combo_cron_enabled'] ): echo 'selected="selected"'; endif; ?> >да</option>
        <option value="0" <?php if ( !isset($saved_settings['combo_cron_enabled']) || ( isset($saved_settings['combo_cron_enabled']) && !$saved_settings['combo_cron_enabled'] )): echo 'selected="selected"'; endif; ?>>нет</option>
    </select>
    <p class="note">Включить cron, будут доступны уведомления и обновления</p>
</div>




<?php if (isset($saved_settings['combo_cron_enabled']) && $saved_settings['combo_cron_enabled'] ) { ?>


    <?php foreach ($cron_config as $cronName=>$cronType)  { ?>

        <div class="form-group">
            <label class="control-label">Cron обновления списка товаров</label>
            <p class="note"><?= $cronType['description']; ?></p>

            <div class="radio">
                <label>
                    <input type="radio" name="cron[<?= $cronName ?>][status]" value="N" <?= ($cronType['value']['inputVal'] == 'N') ? 'checked' : '' ?>  > Не обновлять
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="cron[<?= $cronName ?>][status]"  value="minutes" <?= ($cronType['value']['inputVal'] == 'minutes') ? 'checked' : '' ?>>

                    Каждые <select name="cron[<?= $cronName ?>][minutes]">
                        <option value="5"  <?= (isset($cronType['value']['selectVal']) && $cronType['value']['selectVal'] == 5) ? 'selected' : '' ?> >Каждые 5</option>
                        <option value="10" <?= (isset($cronType['value']['selectVal']) && $cronType['value']['selectVal'] == 10) ? 'selected' : '' ?>>Каждые 10</option>
                        <option value="15" <?= (isset($cronType['value']['selectVal']) && $cronType['value']['selectVal'] == 15) ? 'selected' : '' ?>>Каждые 15</option>
                        <option value="30" <?= (isset($cronType['value']['selectVal']) && $cronType['value']['selectVal'] == 30) ? 'selected' : '' ?>>Каждые 30</option>
                        <option value="60" <?= (isset($cronType['value']['selectVal']) && $cronType['value']['selectVal'] == 60) ? 'selected' : '' ?>>Каждые 60</option>
                    </select>
                    минут (минуты)
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="cron[<?= $cronName ?>][status]" value="everyday" <?= ($cronType['value']['inputVal'] == 'everyday') ? 'checked' : '' ?> >
                    Каждый день в <input type="number" name="cron[<?= $cronName ?>][everyday][hour]" min="0" max="23" value="<?= (isset($cronType['value']['hoursVal']) && $cronType['value']['hoursVal'] ) ?  substr('0'. $cronType['value']['hoursVal'][0], -2) : '' ?>"> : <input type="number" name="cron[<?= $cronName ?>][everyday][min]" value="<?= (isset($cronType['value']['minutesVal']) && $cronType['value']['minutesVal'] ) ? substr('0'.$cronType['value']['minutesVal'][0], -2) : '' ?>" min="0" max="60">
                </label>
            </div>

        </div>



        <?php if( isset($cronType['value']['job']) ) {
            $job = $cronType['value']['job'];
            ?>
            <div class="alert alert-<?= $job['enabled'] ? 'success' : 'warning' ?>">
                <h5>Задача #<?= $job['jobId'] ?></h5>
                <p>Статус: <?= $job['enabled'] ? 'в работе' : 'выключен' ?></p>

                <?php if($job['enabled']) { ?>
                    <p>Следующее выполнение в <strong><?php echo date('Y-m-d H:i:s', $job['nextExecution']); ?></strong> </p>
                <?php } ?>

            </div>

        <?php } ?>

    <?php } ?>


    <div class="form-group">
        <label for="combo_notify_email">Почта для рассылку уведомлений</label><br>
        <input id="combo_notify_email" type="text" name="combo_notify_email" class="form-control" value="<?php if (isset($saved_settings['combo_notify_email'])): echo $saved_settings['combo_notify_email']; endif;?>">
    </div>

    <div class="form-group">
        <label for="combo_notify_error">Уведомлять на почту об ошибка в процессе обновления ? </label><br>

        <select id="combo_notify_error" name="combo_notify_error" class="form-control">
            <option value="1" <?php if (isset($saved_settings['combo_notify_error']) && $saved_settings['combo_notify_error'] ): echo 'selected="selected"'; endif; ?> >да</option>
            <option value="0" <?php if ( !isset($saved_settings['combo_notify_error']) || ( isset($saved_settings['combo_notify_error']) && !$saved_settings['combo_notify_error'] )): echo 'selected="selected"'; endif; ?>>нет</option>
        </select>
        <p class="note"> Если в ходе работы скрипта возникнут ошибки вы будуите уведомлены об этом </p>
    </div>

    <div class="form-group">
        <label for="combo_notify_update_process">Уведомлять на почту о процессе обновления ? </label><br>

        <select id="combo_notify_update_process" name="combo_notify_update_process" class="form-control">
            <option value="1" <?php if (isset($saved_settings['combo_notify_update_process']) && $saved_settings['combo_notify_update_process'] ): echo 'selected="selected"'; endif; ?> >да</option>
            <option value="0" <?php if ( !isset($saved_settings['combo_notify_update_process']) || ( isset($saved_settings['combo_notify_update_process']) && !$saved_settings['combo_notify_update_process'] )): echo 'selected="selected"'; endif; ?>>нет</option>
        </select>
        <p class="note">После синхронизации будет отправлен отчет на email</p>
    </div>


    <div class="form-group">
        <label for="combo_notify_new_offers">Уведомлять на почту о новых офферах если они есть? </label><br>

        <select id="combo_notify_new_offers" name="combo_notify_new_offers" class="form-control">
            <option value="1" <?php if (isset($saved_settings['combo_notify_new_offers']) && $saved_settings['combo_notify_new_offers'] ): echo 'selected="selected"'; endif; ?> >да</option>
            <option value="0" <?php if ( !isset($saved_settings['combo_notify_new_offers']) || ( isset($saved_settings['combo_notify_new_offers']) && !$saved_settings['combo_notify_new_offers'] )): echo 'selected="selected"'; endif; ?>>нет</option>
        </select>
        <p class="note"> Вы будите уведомлены о новых офферах в выбранной зоне на почту указанную выше </p>
    </div>


    <div class="form-group">
        <label for="combo_notify_price_new">Уведомлять об изменении цены ? </label><br>

        <select id="combo_notify_price_new" name="combo_notify_price_new" class="form-control">
            <option value="1" <?php if (isset($saved_settings['combo_notify_price_new']) && $saved_settings['combo_notify_price_new'] ): echo 'selected="selected"'; endif; ?> >да</option>
            <option value="0" <?php if ( !isset($saved_settings['combo_notify_price_new']) || ( isset($saved_settings['combo_notify_price_new']) && !$saved_settings['combo_notify_price_new'] )): echo 'selected="selected"'; endif; ?>>нет</option>
        </select>
        <p class="note"> Вы будите уведомлены о если цены на вашем сейте не соответствуют выгрузке </p>
    </div>


    <hr>


    <h4 >Уведомления в ТГ</h4>

    <div class="form-group">
        <label for="combo_tg_token">Token бота в телеграмм</label><br>
        <input id="combo_tg_token" type="text" name="combo_tg_token" class="form-control" value="<?php if (isset($saved_settings['combo_tg_token'])): echo $saved_settings['combo_tg_token']; endif;?>">
    </div>

    <div class="form-group">
        <label for="combo_tg_users">Ид пользователей кому отсылать уведомления</label><br>
        <input id="combo_tg_users" type="text" name="combo_tg_users" class="form-control" value="<?php if (isset($saved_settings['combo_tg_users'])): echo $saved_settings['combo_tg_users']; endif;?>">
        <p class="note">через запятую, можно узнать <a href="https://t.me/getmyid_bot">@getmyid_bot</a></p>
    </div>



<?php } ?>