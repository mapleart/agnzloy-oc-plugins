<?php

class ModelComboCombo extends Model {

    /**
     * Работа с заказами
     * @return bool
     */
    public function isTableExit(){
        $db = DB_DATABASE;
        $table = DB_PREFIX.'cpa_orders';
        $sql = "SHOW TABLES FROM {$db} LIKE '{$table}' ";
        $query = $this->db->query($sql);
        return !!$query->row;
    }

    public function createTableIfExit(){
        if(!$this->isTableExit()) {
            $db = DB_DATABASE;
            $table = DB_PREFIX.'cpa_orders';
            $sql = "CREATE TABLE `".DB_PREFIX."cpa_orders` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `provider` varchar(100) NOT NULL DEFAULT '',
                `provider_id` int(10) unsigned NOT NULL DEFAULT 0,
                `oc_id` int(10) unsigned NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`),
                KEY `provider` (`provider`),
                KEY `provider_id` (`provider_id`),
                KEY `oc_id` (`oc_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            $query = $this->db->query($sql);
        }
    }

    public function SyncOrder($orderId, $id, $provider) {
        $this->createTableIfExit();
        $table = DB_PREFIX.'cpa_orders';

        $sql = "INSERT INTO `$table` (oc_id, provider, provider_id ) VALUES ";
        $sql .= "('{$orderId}', '{$provider}', '{$id}' )";
        $query = $this->db->query($sql);
        return $query;
    }

    public function getOrderOld($service, $orderId) {
        $this->createTableIfExit();
        $table = DB_PREFIX.'cpa_orders';
        $sql = "SELECT provider_id FROM `$table` WHERE oc_id = {$orderId} AND provider = '{$service}'";
        $query = $this->db->query($sql);
        if($query->row) {
            return $query->row['provider_id'];
        }
        return false;
    }

}
