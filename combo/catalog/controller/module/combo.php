<?php
require_once DIR_SYSTEM.'MapleHelper.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

class ControllerModuleCombo extends Controller
{
    public static function getOffers(){
        try {
            $urlFile = MapleHelper::ExternalStorageGet('combo_offers_file') . 'system/storage/combo.json';

            $json = file_get_contents($urlFile);
            $json = json_decode($json, true);

        } catch (Exception $exception) {
            echo 'Ошибка! Не найден файл парсинга. Пункт 1.1'.PHP_EOL;
            return [];
        }





        $offers = [];
        foreach ($json['offers'] as $offerId => $offer) {
            $offerNew = $offer;
            foreach ($offerNew['prices'] as $key=>$price) {
                $offerNew['prices'][$key]['price'] = ceil( (float)$price['price']);
            }
            $offers[$offerId] = $offerNew;
        }
        return $offers;

    }
    /**
     * Create order on event 2
     *
     * @param int $order_id order identificator
     *
     * @return void
     */
    public function order_create($parameter1, $parameter2 = null)
    {

        $this->load->model('checkout/order');
        $this->load->model('account/order');


        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('combo');
        $status = isset($settings['combo_status']) ? $settings['combo_status'] : null;
        $replaceModel = isset($settings['combo_replace_model']) ? $settings['combo_replace_model'] : null;
        $keyGeo = isset($settings['combo_apigeo']) ? $settings['combo_apigeo'] : null;


        $WEBMASTER_API = isset($settings['combo_webmasterapi']) ? $settings['combo_webmasterapi'] : null;
        ////
        $offers = self::getOffers();
        MapleHelper::sendDebug('test');
        MapleHelper::sendDebug(count($offers));
        ///
        $this->log->write('create order_start');

        if(!$status || !$WEBMASTER_API || !$replaceModel || !$keyGeo) return;



        if($parameter2 != null)
            $order_id = $parameter2;
        else
            $order_id = $parameter1;

        $data = $this->model_checkout_order->getOrder($order_id);

        $data['products'] = $this->model_account_order->getOrderProducts($order_id);
        $comboIds = [];
        $comboPrices = [];
        foreach($data['products'] as $key => $product) {

            $model = $product['model'];
            $pos = strripos($model, $replaceModel);


            if ($pos !== false) {
                $id = (int)str_replace($replaceModel, '', $model);
                if ($id) {
                    $comboIds[] = $id;
                    $comboPrices[$id] = $product['price'];
                }
            }

        }

        // Проверяем на существование заказа в панели
        if(!count($comboIds)){
            return;
        }

        $this->load->model('combo/combo');
        $oldOrder = $this->model_combo_combo->getOrderOld('combo', $order_id);

        if($oldOrder) {
            return false;
        }

        $phone =  isset($data['telephone']) ? $data['telephone'] : '';

        if(!$phone) {
            return;
        }



        $offer_id = $comboIds[0];
        ////
        ///
        ///
        ///

        print_r($offers[$offer_id]);



        $ch = curl_init();



        $utm_source = MapleHelper::getUtm('utm_source');
        $utm_medium = MapleHelper::getUtm('utm_medium');
        $utm_campaign = MapleHelper::getUtm('utm_campaign');
        $utm_term = MapleHelper::getUtm('utm_term');
        $utm_content = MapleHelper::getUtm('utm_content');


        /////
        ///



        $base_url =  (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]";

        $API_URL = "https://api.adcombo.com/api/v2/order/create/";

        $name = '';
        if($data['firstname'] || $data['lastname']) {
            $name = $data['firstname'].' '. $data['lastname'];
        }

        if(!$name) {
            $name = 'Client #' . $order_id;
        }
        $args = [
            'api_key' => $WEBMASTER_API,
            'name' => $name,
            'phone' => isset($data['telephone']) ? $data['telephone'] : '',
            'offer_id' => $offer_id,
            'country_code' => $keyGeo,
            'price' => $comboPrices[$offer_id],
            'base_url' => $base_url,
            'ip' => MapleHelper::getRealIpAddr(),
            'referrer' => $_SERVER['HTTP_REFERER'],

            'utm_campaign' => $utm_campaign,
            'utm_content' => $utm_content,
            'utm_medium' => $utm_medium,
            'utm_source' => $utm_source,
            'utm_term' => $utm_term,
        ];

        $url = $API_URL.'?'.http_build_query($args);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true
        ));
        $res = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($res, true);
        if ($res['code'] == 'ok') {
            $this->model_combo_combo->SyncOrder($order_id, $res['order_id'], 'combo');
        } else {
            ///
            ///
            ///

            MapleHelper::sendDebug($res, "COMBO RESPONSE");
            if ($res['error'] == 'Duplicate order') {
                $this->model_combo_combo->SyncOrder($order_id, 99999999999, 'combo');

            }
        }
    }
}
