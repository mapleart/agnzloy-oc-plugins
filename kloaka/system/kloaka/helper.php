<?php

if(!function_exists('getRequest')) {
    function getRequest($sName, $default = null, $sType = null) {
        $storage = [ 'request'=>$_REQUEST, 'get'=>$_GET, 'post'=>$_POST];
        if($storage and isset($storage[$sType])) {
            $storage = [ $sType=> $storage[$sType]];
        }

        foreach ($storage as $type=>$data) {
            if(isset($data[$sName])) {
                return is_string($data[$sName]) ?  trim($data[$sName]) : $data[$sName];
            }
        }
        return $default;
    }
}


require_once __DIR__.'/PHPExcel.php';
class KloakaHelper {

    static function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    static function generatePriceList($rows){

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        // Add some data
        $sheet->getColumnDimension('A')->setWidth(15);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(30);
        $sheet->getColumnDimension('D')->setWidth(70);
        $sheet->getColumnDimension('E')->setWidth(70);
        $sheet->getColumnDimension('F')->setWidth(10);

        $sheet->setCellValue('A1', '#ID')
            ->setCellValue('B1', 'Фото')
            ->setCellValue('C1', 'Название')
            ->setCellValue('D1', 'Ссылка')
            ->setCellValue('E1', 'Ссылка редирект')
            ->setCellValue('F1', 'Перелинковка');
        $i = 1;


        //29.17 => 180 => 6,060606060606061
       // 9.17 => 60 => 6,543075245365322
       // 59.17 => 360 => 6,084164272435356



        foreach($rows as $product){
            $i++;
            $sheet
                ->setCellValue('A'.$i.'', $product['model'])
                ->setCellValue('C'.$i.'', $product['name'])
                ->setCellValue('D'.$i.'', $product['link'])
                ->setCellValue('E'.$i.'', $product['redirect'])
                ->setCellValue('F'.$i.'', $product['redirect_type']);

            $file = dirname(dirname(__DIR__)).'/'.$product['photo'];


            $sheet->getRowDimension($i)->setRowHeight(60);   // должно сработать на height=auto

            if (file_exists( $file )) {
                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $objDrawing->setName('Photo');
                $objDrawing->setPath($file);
                $objDrawing->setHeight(60);

                $objDrawing->setOffsetX(5); //pixels
                $objDrawing->setOffsetY(5); //pixels

                $objDrawing->setCoordinates('B'.$i.'');
                $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            }
        }

        // Rename worksheet
        $sheet->setTitle('Цены на все товары');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(dirname(dirname(__DIR__)).'/image/kloaka-last.xlsx');
    }

    public static function currentCountry(){
        return self::IpApiCountry();
    }
    public static function IpApiCountry(){
        $ipRes = @file_get_contents('http://ip-api.com/json/'.self::getRealIpAddr());
        $res = @json_decode($ipRes, true);

        if(is_array($res) && isset($res['status']) && $res['status'] == 'success') {
            return mb_strtoupper($res['countryCode']);
        }

        ////
        ///
        $ipRes = @file_get_contents('https://system.trackerlead.biz/data/iso?ip='.self::getRealIpAddr());
        $res = @json_decode($ipRes, true);
        if(is_array($res) && isset($res['country'])) {
            return mb_strtoupper($res['country']);
        }

        return false;
    }





}