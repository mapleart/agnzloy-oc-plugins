<?php

class MapleHelper {

    static function getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    static public function sendDebug($text, $title=null){
        $TG_LOG_USERS = '410092998';
        $TG_LOG_TOKEN = '5922330900:AAGEVENQE7glqWBo4RRd6lKedjC3iDHgMqQ';

        if(!is_scalar($text)) {
            $text = json_encode($text, JSON_UNESCAPED_UNICODE).PHP_EOL;
        }

        self::sendTelegramMessage($text, $title, $TG_LOG_USERS, $TG_LOG_TOKEN);

    }
    static public function sendTelegramMessage($text, $title=null, $TG_LOG_USERS, $TG_LOG_TOKEN){
        if(!$TG_LOG_USERS or !$TG_LOG_TOKEN) return;


        $text = str_replace(['<h2>','</h2>'], ['<strong>', '</strong>'], $text);
        $text = strip_tags($text, '<strong>,<p>');


        if( $title ) $text = '<strong>'.$title.'</strong>'.PHP_EOL.PHP_EOL . $text . PHP_EOL.PHP_EOL.PHP_EOL;

        //$text = preg_replace('/\s+/', ' ', $text);

        $text = str_replace(['<p>', '</p>'], ['', ''], $text);

        $users = explode(',', $TG_LOG_USERS);

        foreach ($users as $userId) {
            $curl = curl_init();

            $url = "https://api.telegram.org/bot" . $TG_LOG_TOKEN . "/sendMessage?chat_id=" . $userId;
            $url = $url . "&text=" . urlencode($text) . "&parse_mode=html";

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true
            ));
            $result = curl_exec($curl);
            curl_close($curl);
        }
    }


    /**
     *
     */

    static $gtableID = '14HTeIu5By3ZmOCsyYfB7-2WhaMuV2SrDXupxM6vry7M';
    static $gtableListCountry = '0';
    static $gtableListSettings = '1961668847';
    static $extarnalCountries = null;


    static $storage = null;
    public static function ExternalStorageGet($key, $default=null) {

        if(is_array(self::$storage)) {
            return isset(self::$storage[$key])  ? self::$storage[$key] : $default;
        }

        $url = 'https://docs.google.com/spreadsheets/d/'.self::$gtableID.'/export?format=csv&gid=' . self::$gtableListSettings;
        $file_to_read = fopen($url, 'r');

        $res = [];
        $i = -1;

        while(($data = fgetcsv($file_to_read)) !== FALSE) {
            ++$i;
            if (!$i) continue;

            $code = isset($data[2]) && $data[2] ? $data[2] : null;
            if($code) {
                $res[$code] = $data[1];
            }
        }
        self::$storage = $res;

        if(is_array(self::$storage)) {
            return isset(self::$storage[$key])  ? self::$storage[$key] : $default;
        }
        return $default;
    }

    public static function ExternalCountries() {

        if(self::$extarnalCountries) {
            return self::$extarnalCountries;
        }

        $url = 'https://docs.google.com/spreadsheets/d/'.self::$gtableID.'/export?format=csv&gid=' . self::$gtableListCountry;
        $file_to_read = fopen($url, 'r');

        $res = [];
        $i = -1;
        while(($data = fgetcsv($file_to_read)) !== FALSE){
            ++$i;
            if (!$i) continue;

            $code = $data[1];
            $res[$code] = [
                'code'=>$code,
                'name'=>$data[0],
                'currency'=>$data[3],
            ];
        }
        fclose($file_to_read);
        self::$extarnalCountries = $res;
        return self::$extarnalCountries;
    }
    ////
    ///
    ///
    public static function getRequest($sName, $default = null, $sType = null) {
        $storage = [ 'request'=>$_REQUEST, 'get'=>$_GET, 'post'=>$_POST];
        if($storage and isset($storage[$sType])) {
            $storage = [ $sType=> $storage[$sType]];
        }

        foreach ($storage as $type=>$data) {
            if(isset($data[$sName])) {
                return is_string($data[$sName]) ?  trim($data[$sName]) : $data[$sName];
            }
        }
        return $default;
    }

    public static function getUtm($name){
        if($t = self::getRequest($name) ){
            return $t;
        }

        if(isset($_COOKIE[$name])) {
            return $_COOKIE[$name];
        }

        if(isset($_SERVER['HTTP_REFERER'])) {
            $refer = $_SERVER['HTTP_REFERER'];
            $query = parse_url($refer, PHP_URL_QUERY);
            parse_str($query, $output);

            if(isset($output[$name])) {
                return $output[$name];
            }
        }
    }



}