<?php echo $header; ?>
<?php echo $column_left;?>

<div id="content" class="plugin-app container-fluid">

    <script id="settings">
        var VUE_SETTINGS = <?= json_encode($vue);  ?>;
    </script>

    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>

    <div id="pluginVue"></div>
</div>


<?php echo $footer; ?>
