// vite.config.js
import vue from '@vitejs/plugin-vue2'

export default {
    css: {
        preprocessorOptions: {
            sass: {
                quietDeps: true
            },
        }
    },
    build: {
        rollupOptions: {
            output: {
                entryFileNames: `assets/[name].js`,
                chunkFileNames: `assets/[name].js`,
                assetFileNames: `assets/[name].[ext]`
            }
        }
    },
    plugins: [vue({
        template: {
            transformAssetUrls: {
                base: null,
                includeAbsolute: false,
            },
        },
    })]
}