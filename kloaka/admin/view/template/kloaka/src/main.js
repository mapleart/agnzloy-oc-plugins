import "./styles/app.scss";
import Vue from "vue";
Vue.prototype.$config = window.docfeedVue;
console.log('Init settings app v2');

import "./plugins/index.js"


import router from './router'


import { vMaska } from "maska"
Vue.directive('maska', vMaska);


window.getConfig = (key, def = '')=>{
    if(typeof VUE_SETTINGS == 'undefined'){
        return def;
    }

    return VUE_SETTINGS[key] || def;
}


///
///
import AppSettings from './SettingsApp.vue'
document.addEventListener('DOMContentLoaded', function() {
/// Приложение главное
    if(document.getElementById('pluginVue')) {
        new Vue({
            render: function (h) { return h(AppSettings) },
            router,
            components: { AppSettings },
        }).$mount('#pluginVue')
    }
});

