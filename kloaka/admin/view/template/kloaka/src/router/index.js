/***************************************/
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)



import Index from '../pages/Index.vue'


let router = new VueRouter( {
    mode: 'hash',
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    },

    routes: [
        { path: '/', name: 'home', component: Index },
    ]
});


export default router;