<?php
require_once dirname(dirname(dirname(__DIR__))).'/system/MapleHelper.php';
require_once dirname(dirname(dirname(__DIR__))).'/system/kloaka/helper.php';
class ControllerModuleKloaka extends Controller
{
    public $apiGeo = ''; // Сразу хранит все гео зоны

    public $status = false;
    public $model = null;

    public function __construct($registry)
    {

       // error_reporting(E_ALL);
        ini_set('display_errors', 0);
        ini_set('default_charset', 'utf-8');

        parent::__construct($registry);
        $this->load->model('tool/image');
        $this->load->model('kloaka/kloaka');
        $this->model = $this->model_kloaka_kloaka;

        $this->getModuleData();
    }

    private $_error = array();
    private $_warning = array();

    /**
     * Install method
     *
     * @return void
     */
    public function install()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('kloaka', array('kloaka_status' => 1));
    }
    /**
     * Uninstall method
     *
     * @return void
     */
    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('kloaka', array('kloaka_status' => 0));

    }

    /**
     * Главная страница
     *
     * @return void
     */


    /***********************
     * Ajax actions
     * @return mixed
     ***********************/
    public function ajax(){
        $method = getRequest('action', 'base');
        if(method_exists(__CLASS__, $method)) {
            return $this->$method();
        }
        return self::error404();
    }

    public function saveSettings(){
        $form = getRequest('form', []);
        foreach ($form as $key=>$val) {
            $key= str_replace('kloaka_', '', $key);
            $this->model->saveSetting($key, $val);
        }
        self::response(['success'=>1, 'message'=>'Настройки сохранены']);
    }
    public function getData(){
        $countries = MapleHelper::ExternalCountries();
        $products = $this->model_kloaka_kloaka->getProductsData();

        $start = microtime(true);


        self::response([
            'current_country'=>\KloakaHelper::currentCountry(),
            'time_country'=> round(microtime(true) - $start, 4),
            'countries'=>$countries,
            'products'=>$products,
            'pluginGeo'=>$this->model->getSetting('geo'),
            'success'=>1,
            'form'=>[
                'kloaka_geo'=>$this->model->getSetting('geo'),
                'kloaka_utm'=>$this->model->getSetting('utm')
            ]
        ]);
    }
    public static function response($data){
        echo json_encode($data);
        die();
    }


    public static function error404(){
        self::response([
            'success'=>0,
            'message'=>'Not found'
        ]);
    }


    /***********************************
     *
     ************************************/
    public function index()
    {

        $this->load->model('setting/setting');
        $this->load->model('extension/module');
        $this->load->model('extension/extension');




        $this->load->language('module/kloaka');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('/admin/view/template/kloaka/dist/assets/index.css');
        $this->document->addScript('/admin/view/template/kloaka/dist/assets/index.js');

        // Обработка дейстаий
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
            $redirect = $this->url->link('module/kloaka', 'token=' . $this->session->data['token'], 'SSL');
            
            // Пример кастомного действия
            if ($this->request->post && isset($this->request->post['form'])) {
                $form = $this->request->post['form'];
                if ($form == 'export'){
                    return $this->export();
                }

                if ($form == 'import'){
                    $this->import();
                    $this->response->redirect($redirect);
                }

                if ($form == 'upgradedb'){
                    $this->upgradeDb();
                    $this->response->redirect($redirect);
                }
            }

            // Сохранение настроек плагина 
            $this->model_setting_setting->editSetting(
                'kloaka',
                $this->request->post
            );

            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($redirect);
        }


        $_data = &$data;


        $_data['kloaka_errors'] = array();
        $_data['saved_settings'] = $this->model_setting_setting
            ->getSetting('kloaka');

        $config_data = array(
            'kloaka_status'
        );



        foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $_data[$conf] = $this->request->post[$conf];
            } else {
                $_data[$conf] = $this->config->get($conf);
            }
        }

        if (isset($this->error['warning'])) {
            $_data['error_warning'] = $this->error['warning'];
        } else {
            $_data['error_warning'] = '';
        }

        $_data['breadcrumbs'] = $this->makeBreadcrumbs();

        $vue['endpoint'] = 'module/kloaka/ajax';
        $vue['token'] =  $this->session->data['token'];


        $_data['cancel'] = $this->url->link(
            'extension/module',
            'token=' . $this->session->data['token'], 'SSL'
        );

        $_data['modules'] = array();

        if (isset($this->request->post['kloaka_module'])) {
            $_data['modules'] = $this->request->post['kloaka_module'];
        } elseif ($this->config->get('kloaka_module')) {
            $_data['modules'] = $this->config->get('kloaka_module');
        }

        $this->load->model('design/layout');
        $_data['token'] = $this->session->data['token'];
        $_data['layouts'] = $this->model_design_layout->getLayouts();
        $_data['header'] = $this->load->controller('common/header');
        $_data['column_left'] = $this->load->controller('common/column_left');
        $_data['footer'] = $this->load->controller('common/footer');

        if (isset($this->session->data['success'])) {
            $data['alert_notice'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['alert_notice'] = '';
        }

        if (isset($this->session->data['error'])) {
            $data['error_notice'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_notice'] = '';
        }
        $productUrls = isset($_data['saved_settings']['kloaka_product_redirects']) ? $_data['saved_settings']['kloaka_product_redirects'] : [];
        $redirectTypes = isset($_data['saved_settings']['kloaka_product_redirect_type']) ? $_data['saved_settings']['kloaka_product_redirect_type'] : [];



        $_data['vue'] = $vue;
        //$this->sync();

        $this->response->setOutput(
            $this->load->view('module/kloaka.tpl', $_data)
        );
    }

    public function saveProduct(){
        $this->load->model('catalog/product');
        $product_info = $this->model_catalog_product->getProduct(getRequest('product_id'));

        if(!$product_info) {
            echo json_encode(['success'=>0]);
            return;
        }

        $this->load->model('localisation/language');

        $settings = $this->model_setting_setting->getSetting('kloaka');
        //  = ''; // хранит id => ссылка
        // $settings['kloaka_product_redirect_type'] = ''; // id => тип

        $productId = $product_info['product_id'];
        $settings['kloaka_product_redirects'][$productId] = getRequest('link');
        $settings['kloaka_product_redirect_type'][$productId] = getRequest('linkType');


        $this->model_setting_setting->editSetting(
            'kloaka',
            $settings
        );

        self::response(['success'=>1, 'message'=>'Сохранено']);

    }


    public function upgradeDb(){
        $this->load->model('catalog/product');
        $this->load->model('localisation/language');

        // ALTER TABLE `setting` MODIFY COLUMN `value` LONGTEXT NOT NULL
        $this->model_kloaka_kloaka->upgradeDb();

        $redirect = $this->url->link('module/kloaka', 'token=' . $this->session->data['token'], 'SSL');

        $this->session->data['success'] = 'База данных в актуальном состоянии';
        $this->response->redirect($redirect);
    }
    public function exportDump(){

        $this->session->data['success'] = $this->language->get('text_success');
        $settings = $this->model_setting_setting->getSetting('kloaka');

        $productUrls = isset($settings['kloaka_product_redirects']) ? $settings['kloaka_product_redirects'] : [];
        $redirectTypes = isset($settings['kloaka_product_redirect_type']) ? $settings['kloaka_product_redirect_type'] : [];
        $productsRaw = $this->model_kloaka_kloaka->getProducts();

        $products = [];
        foreach ($productsRaw as $productRaw) {
            $productId = $productRaw['product_id'];
            $productUrl = $this->model_kloaka_kloaka->getProductUrlFromStore($productId)[0]['href'];
            $productRedirect = isset($productUrls[$productId]) ? $productUrls[$productId] : '';
            $productRedirectType = isset($redirectTypes[$productId]) ? $redirectTypes[$productId] : 'redirect';
            $productImage = $this->model_tool_image->resize($productRaw['image'], 120, 120);



            $productRedirect = str_replace('&amp;', '&', $productRedirect);




            $productImage = str_replace([ HTTPS_CATALOG, HTTP_CATALOG ] , ['', ''], $productImage);
            $products[$productId] = [
                'product_id'=>$productRaw['model'],
                'photo'=>$productImage,
                'name'=>$productRaw['name'],
                'model'=>$productRaw['model'],
                'link'=>$productUrl,
                'redirect'=>$productRedirect,
                    'redirect_type'=>$productRedirectType,


            ];
        }

        KloakaHelper::generatePriceList($products);

        self::response([
            'success'=>1,
            'file'=>($this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG).'image/kloaka-last.xlsx',
        ]);
    }

    public function importDump(){
        $start = microtime(true);

        $this->load->model('catalog/product');
        $this->load->model('localisation/language');
        $redirect = $this->url->link('module/kloaka', 'token=' . $this->session->data['token'], 'SSL');


        $settings = $this->model_setting_setting->getSetting('kloaka');

        if(!isset($_FILES['file'])) {
           self::response([
                'success'=>0,
                'message'=>'Загрузите файл'
            ]);
        }

        $filename = $_FILES['file']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);

        if($ext != 'xlsx'){
             self::response([
                'success'=>0,
                'message'=>'Неверный формат файла'
            ]);
        }
        $inputFile = $_FILES['file']['tmp_name'];

        //Read spreadsheeet workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFile);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $phpexcel_objPHPExcel = $objReader->load($inputFile);
        } catch(Exception $e) {
            self::response([
                'success'=>0,
                'message'=>'Файл не получен'
            ]);
         //   $this->session->data['error'] = 'Системная ошибка: '. $e->getMessage();
          //  return $this->response->redirect($redirect);
        }


        // convert one sheet
        $phpexcel_sheet = $phpexcel_objPHPExcel->getSheet(0);
        $phpexcel_highestRow = $phpexcel_sheet->getHighestRow();
        $phpexcel_highestColumn = $phpexcel_sheet->getHighestColumn();
        $phpexcel_array = $phpexcel_sheet->toArray();
        $head = array_shift($phpexcel_array); // Без шапки

        if($head[0] != '#ID') {
            self::response([
                'success'=>0,
                'message'=>'Неверный формат содержимого'
            ]);
        }

        $productsRaw = $this->model_kloaka_kloaka->getProducts();
        $productModals = [];
        foreach ($productsRaw as $item) {
            $productModals[ $item['model'] ] =  $item['product_id'];
        }
//
        foreach ($phpexcel_array as $data) {
            $model = $data[0];
            if(!isset($productModals[$model])) continue;

            $productId = $productModals[$model];

            $link = $data[4];
            $linkType = $data[5];


            $link = str_replace('&amp;', '&', $link);


            $settings['kloaka_product_redirects'][$productId] = $link;
            $settings['kloaka_product_redirect_type'][$productId] = $linkType;



        }

        $this->model_setting_setting->editSetting(
            'kloaka',
            $settings
        );

        self::response(['success'=>1, 'message'=>'Успешно импортировано '.count($phpexcel_array) . ' записей. Скрипт был выполнен за ' . (microtime(true) - $start) . ' секунд']);
    }

    /**
     * Создание переменных для модуля
     * @return false
     */
    public function getModuleData() {

        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('kloaka');

        $this->status = isset($settings['kloaka_status']) ? $settings['kloaka_status'] : null;
         $this->apiGeo = isset($settings['kloaka_utm']) ? $settings['kloaka_utm'] : null;
    }

    /**
     * Validate
     *
     * @return bool
     */
    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/kloaka')) {
            $this->_error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->_error) {
            return true;
        } else {
            return false;
        }
    }


    public function makeBreadcrumbs(){

        $res[] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link(
                'common/home',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => false
        );

        $res[] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link(
                'extension/module',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        $res[] = array(
            'text'      => $this->language->get('kloaka_title'),
            'href'      => $this->url->link(
                'module/kloaka',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        return $res;
    }
}
