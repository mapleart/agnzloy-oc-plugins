<?php

// Heading Goes here:
$_['heading_title']          = 'Перелинковка';
$_['dropka_title']            = 'Перелинковка';

// Text
$_['text_module']                = 'Модули';
$_['text_success']               = 'Настройки успешно сохранены';
$_['text_notice']                = 'Внимание! Модель товара должены совпадать с id в выгрузке';

// Errors
$_['error_permission']           = 'У вас недостаточно прав на изменение настроек модуля';

