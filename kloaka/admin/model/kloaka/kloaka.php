<?php

class ModelKloakaKloaka extends Model {


    public function upgradeDb() {
        $sql = 'ALTER TABLE `setting` MODIFY COLUMN `value` LONGTEXT NOT NULL';
        return $this->db->query($sql);

    }

    /**
     * Вернет нужные данные для плагина по товарам
     * @param null $replaceModel
     * @return array
     */
    public function getProductsData($replaceModel = null) {
        $res = [];



        $productUrls = self::getSetting('product_redirects', []);
        $redirectTypes = self::getSetting('product_redirect_type', []);;


        $productsRaw = $this->getProducts($replaceModel);
        foreach ($productsRaw as $productRaw) {
            $productId = $productRaw['product_id'];
            $productUrl = $this->model_kloaka_kloaka->getProductUrlFromStore($productId)[0]['href'];
            $productRedirect = isset($productUrls[$productId]) ? $productUrls[$productId] : '';
            $productRedirectType = isset($redirectTypes[$productId]) ? $redirectTypes[$productId] : 'curl';
            $productImage = $this->model_tool_image->resize($productRaw['image'], 120, 120);
            $res[$productId] = array_merge($productRaw, [
                'product_url'=>$productUrl,
                'product_redirect'=>str_replace('&amp;', '&', $productRedirect),
                'product_redirect_type'=>$productRedirectType,
                'image'=>$productImage]);
        }

        return $res;
    }
    public function getProducts($replaceModel = null) {
        $sql = "SELECT p.product_id, p.model, image, pd.name, status,
        (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special
        
        FROM " . DB_PREFIX . "product p
        LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
        ";

        if( $replaceModel ) {
            $sql .= " WHERE p.model LIKE '%" . $replaceModel . "%'";
        }

        $sql .=' order by status desc';
        $query = $this->db->query($sql);

        $res = [];
        foreach ($query->rows as $row) {
            $model = $row['model'];
            $correct = false;

            $prefixes = ['kma-', 'm1shop-', 'trafficlight-', 'combo-'];
            foreach ($prefixes as $prefix) {
                if(strpos($model, $prefix) !== false) {
                    $correct = true;
                }
            }

            if($correct) {
                $res[] = $row;
            }

        }
        return $res;
    }


    public function getProduct($model) {
        $sql = "SELECT p.product_id, p.model,
        (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special
        FROM " . DB_PREFIX . "product p
        ";

        if( $model ) {
            $sql .= " WHERE p.model = '" . $model . "'";
        }

        $query = $this->db->query($sql);
        return $query->row;
    }



    /**
     * Ипорт
     */
    public function uploadImage($sFileUrl, $sDirSave){
        if (!$aImageInfo = (@getimagesize($sFileUrl))) {
            return false;
        }
        $aTypeImage = array(
            1 => 'gif',
            2 => 'jpg',
            3 => 'png'
        ); // see http://php.net/manual/en/function.exif-imagetype.php
        $sExtension = isset($aTypeImage[$aImageInfo[2]]) ? $aTypeImage[$aImageInfo[2]] : 'jpg';


        $rFile = fopen($sFileUrl, 'r');
        if (!$rFile) {
            return false;
        }

        $iSizeKb = 0;
        $sContent = '';
        while (!feof($rFile) and $iSizeKb < 1024 * 15) {
            $sContent .= fread($rFile, 1024 * 2);
            $iSizeKb++;
        }
        /**
         * Если конец файла не достигнут,
         * значит файл имеет недопустимый размер
         */
        if (!feof($rFile)) {
            return false;
        }
        fclose($rFile);
        /**
         * Копируем загруженный файл
         */
        if (!is_dir($sDirSave)) {
            @mkdir($sDirSave, 0777, true);
        }

        $fileName = substr(md5(uniqid(mt_rand(), true)), 0, 10) . '.' . $sExtension;
        $sFileTmp = $sDirSave . $fileName;
        $rFile = fopen($sFileTmp, 'w');
        fwrite($rFile, $sContent);
        fclose($rFile);
        return  $fileName;
    }

    public function makeSlug($title, $separator = '-')
    {
        $matrix = [
            'й' => 'i',    'ц' => 'c',  'у' => 'u',  'к' => 'k',    'е' => 'e',
            'н' => 'n',    'г' => 'g',  'ш' => 'sh', 'щ' => 'shch', 'з' => 'z',
            'х' => 'h',    'ъ' => '',   'ф' => 'f',  'ы' => 'y',    'в' => 'v',
            'а' => 'a',    'п' => 'p',  'р' => 'r',  'о' => 'o',    'л' => 'l',
            'д' => 'd',    'ж' => 'zh', 'э' => 'e',  'ё' => 'e',    'я' => 'ya',
            'ч' => 'ch',   'с' => 's',  'м' => 'm',  'и' => 'i',    'т' => 't',
            'ь' => '',     'б' => 'b',  'ю' => 'yu', 'ү' => 'u',    'қ' => 'k',
            'ғ' => 'g',    'ә' => 'e',  'ң' => 'n',  'ұ' => 'u',    'ө' => 'o',
            'Һ' => 'h',    'һ' => 'h',  'і' => 'i',  'ї' => 'ji',   'є' => 'je',
            'ґ' => 'g',    'Й' => 'I',  'Ц' => 'C',  'У' => 'U',    'Ұ' => 'U',
            'Ө' => 'O',    'К' => 'K',  'Е' => 'E',  'Н' => 'N',    'Г' => 'G',
            'Ш' => 'SH',   'Ә' => 'E',  'Ң '=> 'N',  'З' => 'Z',    'Х' => 'H',
            'Ъ' => '',     'Ф' => 'F',  'Ы' => 'Y',  'В' => 'V',    'А' => 'A',
            'П' => 'P',    'Р' => 'R',  'О' => 'O',  'Л' => 'L',    'Д' => 'D',
            'Ж' => 'ZH',   'Э' => 'E',  'Ё' => 'E',  'Я' => 'YA',   'Ч' => 'CH',
            'С' => 'S',    'М' => 'M',  'И' => 'I',  'Т' => 'T',    'Ь' => '',
            'Б' => 'B',    'Ю' => 'YU', 'Ү' => 'U',  'Қ' => 'K',    'Ғ' => 'G',
            'Щ' => 'SHCH', 'І' => 'I',  'Ї' => 'YI', 'Є' => 'YE',   'Ґ' => 'G',
        ];

        foreach ($matrix as $from => $to)  {
            $title = mb_eregi_replace($from, $to, $title);
        }

        $pattern = '![^'.preg_quote($separator).'\pL\pN\s]+!u';
        $title = preg_replace($pattern, '', mb_strtolower($title));

        $flip = $separator == '-' ? '_' : '-';

        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);

        return trim($title, $separator);
    }


    public function getProductUrlFromStore($productID){
        $this->load->model('setting/store');
        $this->load->model('catalog/product');

        if (version_compare(VERSION, '2.2.0', '>=')) {
            $_url = new Url($this->config->get('config_secure') ? HTTP_CATALOG : HTTPS_CATALOG);
            $_url->setDomain(HTTP_CATALOG);
        } else {
            $_url = new Url(HTTP_CATALOG, $this->config->get('config_secure') ? HTTP_CATALOG : HTTPS_CATALOG);
        }
        if ($this->config->get('config_seo_url')) {
            if (version_compare(VERSION, '2.2.0', '>=')) {
                if (class_exists('VQMod')) {
                    require_once(VQMod::modCheck((DIR_APPLICATION . '../catalog/controller/startup/seo_url.php')));
                } else {
                    // TODO: try loading OCMODed file
                    require_once(DIR_APPLICATION . '../catalog/controller/startup/seo_url.php');
                }
                $seo_url = new ControllerStartupSeoUrl($this->registry);
            } else {
                // Require the SEO class directly from relative path to /admin/index.php
                if (class_exists('VQMod')) {
                    require_once(VQMod::modCheck((DIR_APPLICATION . '../catalog/controller/common/seo_url.php')));
                } else {
                    require_once(DIR_APPLICATION . '../catalog/controller/common/seo_url.php');
                }
                $seo_url = new ControllerCommonSeoUrl($this->registry);
            }
        }

        $data['stores'][0] = array(
            'name'  => $this->config->get('config_name'),
            'url'   => HTTP_CATALOG,
            'ssl'   => HTTPS_CATALOG,
            'make'  => $_url
        );
        $stores = $this->model_setting_store->getStores();

        foreach ($stores as $store) {
            $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store['store_id'] . "' AND `key` = 'config_secure'");
            $secure = (int)$query->row['value'];

            if (version_compare(VERSION, '2.2.0', '>=')) {
                $__url = new Url($secure ? $store['url'] : $store['ssl']);
                $__url->setDomain($store['url']);
            } else {
                $__url = new Url($store['url'], $secure ? $store['url'] : $store['ssl']);
            }

            $data['stores'][$store['store_id']] = array(
                'name'  => $store['name'],
                'url'   => $store['url'],
                'ssl'   => $store['ssl'],
                'make'  => $__url
            );

            if ($this->config->get('config_seo_url')) {
                $data['stores'][$store['store_id']]['make']->addRewrite($seo_url);
            }
        }

        $product_stores = $this->model_catalog_product->getProductStores($productID);
        $row = [];

        foreach ($product_stores as $store) {
            if (!in_array($store, array_keys($data['stores'])))
                continue;
            $row[] = array(
                'name' => $data['stores'][$store]['name'],
                'href' => $data['stores'][$store]['make']->link('product/product&product_id=' . $productID)
            );
        }
        return $row;
    }


    private function sendMail( $subject, $html, $file = '')
    {
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->notify_email);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);

        if($file) {
            $mail->addAttachment($file);   // I took this from the phpmailer example on github but I'm not sure if I have it right.
        }

        $mail->setHtml($html);
        $mail->send();
    }








    const module = 'kloaka';

    /**
     * Сохраняет значение плагина
     * @param $key
     * @param $value
     * @param null $group
     */
    public function saveSetting($key, $value, $group = null){
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting(self::module);


        if($group) {
            $settings[self::module.'_'.$group][$key] = $value;
        } else {
            $settings[self::module.'_'.$key] = $value;
        }

        $this->model_setting_setting->editSetting(
            self::module,
            $settings
        );
    }

    /**
     * Получает значение плагина по ключу
     * @param $key
     * @param null $default
     * @param null $group
     * @return mixed|null
     */
    public function getSetting($key, $default = null, $group = null){
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting(self::module);

        if($group) {

            if(isset($settings[self::module.'_'.$group][$key]) && $settings[self::module.'_'.$group][$key]) {
                return $settings[self::module.'_'.$group][$key];
            }

            return $default;
        }

        if(isset($settings[self::module.'_'.$key]) && $settings[self::module.'_'.$key]) {
            return $settings[self::module.'_'.$key];
        }
        return $default;

    }
}
