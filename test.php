<?php


public function getimgsize( $url, $referer = '' ) {
    // Set headers
    $headers = array( 'Range: bytes=0-131072' );
    if ( !empty( $referer ) ) { array_push( $headers, 'Referer: ' . $referer ); }

    // Get remote image
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
    $headers = array();
    $headers[] = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $data = curl_exec( $ch );
    $http_status = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
    $curl_errno = curl_errno( $ch );
    curl_close( $ch );

    // Get network stauts
    if ( $http_status != 200 ) {
        echo 'HTTP Status[' . $http_status . '] Errno [' . $curl_errno . ']';
        return [0,0];
    }

    // Process image
    $image = imagecreatefromstring( $data );
    $dims = [ imagesx( $image ), imagesy( $image ) ];
    imagedestroy($image);

    return $dims;
}


 function download($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $path_parts = pathinfo($url); //получаем название файла из URL
    $fp = fopen(__DIR__.'/'.$path_parts['basename'], 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);


    $headers = array();
    $headers[] = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
}

$rest = getimgsize('https://kma.biz/uploads/store/6bb24468956384c482a8b5a901fb6383/0ac5a932c04b6232709d6b66ced3cab03c418555.png');
print_r($rest);