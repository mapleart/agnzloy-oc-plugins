<?php

$ver = '531';


if(is_dir(__DIR__.'/@generated/')) {
    rrmdir(__DIR__.'/@generated/');
}
@mkdir(__DIR__.'/@generated', 0777, true);

///
/// Создаем модули
///
//foreach (['targets', 'm1shop', 'cpatl', 'kmabiz', 'combo','kloaka', 'smartfeed'] as $module) {
foreach (['m1shop', 'kloaka'] as $module) {
    ///
    rrmdir(__DIR__.'/tmp/');

    ///
    if(is_dir(__DIR__.'/tmp/upload/')) {
        rrmdir(__DIR__.'/tmp/upload');
    }
    @mkdir(__DIR__.'/tmp/upload', 0777, true);

    recurseCopy(__DIR__.'/'.$module.'/', __DIR__.'/tmp/upload');

    if(is_dir(__DIR__.'/tmp/upload/admin/view/template/' . $module .'/node_modules')) {
        rrmdir(__DIR__.'/tmp/upload/admin/view/template/' . $module .'/node_modules');
    }

    Zip(__DIR__.'/tmp', __DIR__.'/@generated/'.$module.'_v'.$ver.'.ocmod.zip');

    echo 'Module created: '. $module .' file: '. $module.'_v'.$ver.'.ocmod.zip'.PHP_EOL;
}


// Удаляем все временное
rrmdir(__DIR__.'/tmp');

///
///
///

function recurseCopy(string $sourceDirectory, string $destinationDirectory, string $childFolder = ''): void {
    $directory = opendir($sourceDirectory);

    if (is_dir($destinationDirectory) === false) {
        mkdir($destinationDirectory);
    }

    if ($childFolder !== '') {
        if (is_dir("$destinationDirectory/$childFolder") === false) {
            mkdir("$destinationDirectory/$childFolder", '');
        }

        while (($file = readdir($directory)) !== false) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            if (is_dir("$sourceDirectory/$file") === true) {
                recurseCopy("$sourceDirectory/$file", "$destinationDirectory/$childFolder/$file");
            } else {
                copy("$sourceDirectory/$file", "$destinationDirectory/$childFolder/$file");
            }
        }

        closedir($directory);

        return;
    }

    while (($file = readdir($directory)) !== false) {
        if ($file === '.' || $file === '..') {
            continue;
        }

        if (is_dir("$sourceDirectory/$file") === true) {
            recurseCopy("$sourceDirectory/$file", "$destinationDirectory/$file");
        }
        else {
            copy("$sourceDirectory/$file", "$destinationDirectory/$file");
        }
    }

    closedir($directory);
}



function Zip($source, $destination)
{
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            if (is_dir($file) === true)
            {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}
function rrmdir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
                    rrmdir($dir. DIRECTORY_SEPARATOR .$object);
                else
                    unlink($dir. DIRECTORY_SEPARATOR .$object);
            }
        }
        rmdir($dir);
    }
}

