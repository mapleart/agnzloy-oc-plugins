#!/usr/bin/python
# -*- coding: UTF-8 -*-
from urllib2 import urlopen
import json
import os, time
from datetime import datetime
#from fuzzywuzzy import fuzz
from difflib import SequenceMatcher as SM
import requests
import pandas as pd
import re
import sys



######
start_time = time.time()
jsonRes = {
    'start_time': start_time,
    'countries': []
}

jsonResGlobal = {
    "success": 0
}
######



def getCurrencyConfig():
    gtableId = '14HTeIu5By3ZmOCsyYfB7-2WhaMuV2SrDXupxM6vry7M'
    gtableListId = '0'
    storageUrl = 'https://docs.google.com/spreadsheets/d/'+gtableId+'/export?format=csv&gid=' + gtableListId
    response = urlopen(storageUrl)
    cr = pd.read_csv( storageUrl )
    cr = cr.to_numpy()

    res = {}
    i = 0
    for row in cr:

        i +=1
        if i > 1 :
            res[ row[1] ] = row[3]

    return res

def sendTG(message):
    TOKEN = "5922330900:AAGEVENQE7glqWBo4RRd6lKedjC3iDHgMqQ"
    chat_id = "410092998"
    url = "https://api.telegram.org/bot"+TOKEN+"/sendMessage?chat_id="+chat_id+"&text="+message
    requests.get(url)


sendTG('start parsing M1shop')

reload(sys)
sys.setdefaultencoding('utf-8')

os.environ['TZ'] = 'Europe/Moscow'
time.tzset()

start_time = time.time()
import multiprocessing
import time

removeArr = ['( LOW Price)', '(low price)', '(Low Price)', '( Full Price)', 'Full Price', 'Low Price']


SimilarFindedAll = {}
SimilarFindedM1 = {}
SimilarFindedCPA = {}


def clearString(strVal):
    orig = strVal
    strVal = strVal.lower()
    for strR in removeArr:
        strVal = strVal.replace(strR, '')
        strVal = strVal.replace(strR.lower(), '')

    return strVal




def checkSimilarFinal(id, name, shortName, prices, offers):
    similars = checkSimilar(id, name, shortName, prices, offers)

    SimilarFindedM1[ id ] = similars

    for similar in similars:
        if similar['idCpa'] not in SimilarFindedCPA:
            SimilarFindedCPA[similar['idCpa']] = []
        SimilarFindedCPA[ similar['idCpa'] ].append(similar)



    return similars


def checkSimilar(id, name, shortName, prices, offers):
    name = clearString(name)
    similar = []
    #  ищем полные совпадения

    for key in offers:
        similarOffer = offers[key]
        name2 = similarOffer['name']
        name2Orig = similarOffer['name']

        name2 = clearString(name2)



        if(name2.strip() == name.strip() ):
            dataSimilar = {'id': key, 'name': name2Orig, 'percent': 1.0, 'idCpa': key, 'idM1': id, 'pricesM1': prices, 'pricesCpa': similarOffer['prices'] }
            similar.append( dataSimilar )

    if( len(similar) > 0):
        return similar

    similarTest = []
    for key in offers:
        similarOffer = offers[key]
        name2 = similarOffer['name']



        if(shortName):
            percent = checkString(name2, shortName)
            if(percent > 0.6):
                similarTest.append( {'id': key, 'name': name2, 'percent': percent, 'idCpa': key, 'idM1': id, 'pricesM1': prices, 'pricesCpa': similarOffer['prices'] } )


    if(len(similarTest) > 1):
        for item in similarTest:
            name2 = clearString( item['name'])
            percent = SM(None, name2, name).ratio()

            if(percent > 0.5):
                similar.append( item )
    else:
        similar = similarTest

    return similar

def checkString(s1, s2):

    s1 = clearString(s1)
    s2 = clearString(s2)

    ar1 = s1.strip().split(' ')
    ar2 = s2.strip().split(' ')
    result=list(set(ar1) & set(ar2))

    result = float(len(result))
    base = float(len(ar2))


    if( result > 0 ):
        return  ((result*100) / (base * 100))
    return 0


countryCurrencies = getCurrencyConfig()
def buildCurrency(geo):
    if geo not in countryCurrencies:
        if geo not in jsonRes['countries']:
            jsonRes['countries'].append(geo)
            sendTG( 'Добавьте страну '  + geo )
    else:
        return countryCurrencies[geo]



def parse():

    url = 'http://m1.top/offers_export_api/?webmaster_id=50177&api_key=86dfb4dbb4d0c3f0930aca28e40edb94'

    response = urlopen(url)
    data = response.read().decode('UTF-8')
    dataNew = json.loads(data)


    # Для сравнения
    urlSimple = 'https://georgia-market.ru/system/storage/cpadata.json'
    responseSimple = urlopen(urlSimple)
    dataSimple = responseSimple.read().decode('UTF-8')
    dataSimpleArr = json.loads(dataSimple)



    #with open( os.path.dirname(os.path.realpath(__file__))+'/data_test.json') as data_json:
    #    dataNew  = json.load(data_json)
    result = {}
    reg = re.compile('[^a-zA-Z ]')

    i = 0
    for offer in dataNew:
        i = i + 1
        id = offer['product_id']
        if not result.has_key(id):
            result[id] = {}

        prices = {}
        titles = {}
        titles2 = {}
        geos = []


       # for land in offer['landings']:
       #     titles[land['language_code']] = land['title']


        for goal in offer['target']:

            currency = goal['currency']
            if not currency:
                currency = buildCurrency( goal['code'] )

            prices[currency] = {
                'price': goal['price'],
                'geo': goal['code'],
            }

            titles[goal['code']] = goal['geo_name']
            geos.append( goal['code'] )




        result[ id ]['prices'] = prices
        result[ id ]['geos'] = geos
        #result[ id ]['titles'] = titles
        #result[ id ]['titles2'] = titles2

        result[ id ]['name'] = offer['name']

        short = reg.sub('', offer['name'])
        short = re.sub(r'\s+', ' ', short)
        short = short.strip()
        result[ id ]['name_short'] = short
        result[ id ]['url']  = ''
        result[ id ]['picture_url']  = offer['img']
        result[ id ]['category']  = ''
        result[ id ]['description']  = offer['info']



        result[ id ]['similar'] = checkSimilarFinal(id, offer['name'], short, prices, dataSimpleArr['offers'])

    jsonRes['time'] = time.strftime('%Y') + '-' +  time.strftime('%m') + '-' + time.strftime('%d') + ' ' + time.strftime('%H') + ':' + time.strftime('%M')
    jsonResGlobal['processed'] = jsonRes['processed'] = time.time() - jsonRes['start_time']
    jsonRes['offers'] = result
    jsonRes['offers_count'] = len(result)
    jsonResGlobal['offers_count'] = len(result)
    jsonResGlobal['time'] = jsonRes['time']
    jsonResGlobal['countries'] = jsonRes['countries']

    with open( os.path.join(   os.path.dirname(os.path.dirname(os.path.realpath(__file__))) , "storage/m1shop.json") , 'w') as f:
        json.dump( jsonRes, f, indent=4, ensure_ascii=False)

    with open( os.path.join(   os.path.dirname(os.path.dirname(os.path.realpath(__file__))) , "storage/similar.json") , 'w') as f:
        json.dump( { 'm1shop': SimilarFindedM1, 'cpa': SimilarFindedCPA }, f, indent=4, ensure_ascii=False)



parse()


message = 'M1shop end script '

jsonResGlobal['success'] = 1
json_object = json.dumps(jsonResGlobal, indent = 4)
sendTG(message + '\n' + json_object)
print(json_object)

