<?php
require_once DIR_SYSTEM.'MapleHelper.php';
class ModelM1shopM1shop extends Model {
    const module = 'm1shop';


    public function patchTable() {
        $sql = "ALTER TABLE " . DB_PREFIX . "product MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "cart MODIFY `product_id` BIGINT";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "coupon_product MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "customer_wishlist MODIFY product_id BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "order_product MODIFY product_id BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "order_recurring MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_attribute MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_description MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_discount MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_filter MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_image MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_option MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_option_value MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_recurring MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_related MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_reward MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_special MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_to_category MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_to_download MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_to_layout MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "product_to_store MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE `" . DB_PREFIX . "return` MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);

        $sql = "ALTER TABLE " . DB_PREFIX . "review MODIFY `product_id` BIGINT;";
        $query = $this->db->query($sql);
       // $query = $this->db->query($sql);

    }
    public function getProducts($replaceModel) {
        $sql = "SELECT p.product_id, p.model,
        (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special
        FROM " . DB_PREFIX . "product p
        ";

        if( $replaceModel ) {
            $sql .= " WHERE p.model LIKE '%" . $replaceModel . "%'";
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }


    public function getProduct($model) {
        $sql = "SELECT p.product_id, p.model,
        (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special
        FROM " . DB_PREFIX . "product p
        ";

        if( $model ) {
            $sql .= " WHERE p.model = '" . $model . "'";
        }

        $query = $this->db->query($sql);
        return $query->row;
    }



    public function hideAllProducts($replaceModel, $allProducts) {
        $sql = "UPDATE " . DB_PREFIX . "product SET status = 0 ";

        if( $replaceModel ) {
            $sql .= " WHERE model LIKE '%" . $replaceModel . "%'";
        }

        $query = $this->db->query($sql);
        $this->cache->delete('product');

        foreach ($allProducts as $productData) {
          //print_r($productData['product_id']);
            // $this->event->trigger('post.admin.product.delete', );
        }


        return $query;

    }

    public function updatePrice($product_id, $price, $special) {
        if(!$price) return;

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '1', priority = '0', price = '" . (float)$special . "', date_start = '', date_end = ''");

        $this->db->query("UPDATE " . DB_PREFIX . "product SET price = '".$price."' WHERE product_id = '".$product_id."' ");

    }
    public function process($ids) {
        foreach ($ids as $id ) {
            $sql = "UPDATE " . DB_PREFIX . "product SET status = 1 WHERE product_id = {$id}";
            $this->db->query($sql);
        }
    }


    public function checkEvent($code, $trigger, $action){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "event WHERE `code` = '" . $this->db->escape($code) . "' AND `trigger` = '" . $this->db->escape($trigger) . "' AND `action` = '" . $this->db->escape($action) . "'");
        return $query->row;
    }




    /////
    /////
    /////

    /**
     * СОхраняет значение плагина
     * @param $key
     * @param $value
     * @param null $group
     */
    public function saveSetting($key, $value, $group = null){
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting(self::module);


        if($group) {
            $settings[self::module.'_'.$group][$key] = $value;
        } else {
            $settings[self::module.'_'.$key] = $value;
        }

        $this->model_setting_setting->editSetting(
            self::module,
            $settings
        );
    }

    public function getSetting($key, $default = null, $group = null){
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting(self::module);

        if($group) {

            if(isset($settings[self::module.'_'.$group][$key]) && $settings[self::module.'_'.$group][$key]) {
                return $settings[self::module.'_'.$group][$key];
            }

            return $default;
        }

        if(isset($settings[self::module.'_'.$key]) && $settings[self::module.'_'.$key]) {
            return $settings[self::module.'_'.$key];
        }
        return $default;

    }

    ////
    ////
    ////


    public function CronApi($url, $params = [], $requestType = 'GET'){
        $token = MapleHelper::ExternalStorageGet('cronjobs_api_key');
        $endpoint = 'https://portugal-the-market1.su/api/'.trim($url,'/');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $requestType);



        if(count($params)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));

        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type:application/json',
            'Authorization: Bearer '. $token
        ]);

        $server_output = curl_exec ($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        if($httpcode == 200) {
            return [
                'success'=>1,
                'result'=>json_decode($server_output, true)
            ];
        }



        $error = 'Endpoint: ' . $endpoint . PHP_EOL;
        $error .= 'Params: ' . json_encode($params) . PHP_EOL;
        $error .= 'Request: ' . $requestType . PHP_EOL;;
        $error .= 'Output: ' . $server_output . PHP_EOL;

        MapleHelper::sendDebug($error, '[ M1SHOP '.HTTPS_CATALOG.'] Error '. $httpcode .  ' -- Authorization: Bearer '. $token );
        return [
            'success'=>0,
            'error'=>$httpcode
        ];


    }



    public function GetJobsTasks(){
        return [
            /**
             * Проверяет выгрузку / скрывает и открывает товары
             */
            'sync'=>[
                'event'=>'sync',
                'description'=>'Проверяет выгрузку, скрывает/открывает товары, обновляет цены, добавляет товары',
                'value'=>$this->getJobValue('sync'),
            ]
        ];
    }

    public function getJobValue($type) {
        //

        $taskId = $this->getSetting($type, null, 'cron');

        if(!$taskId) {
            $urlBase = HTTPS_CATALOG . "system/cron/".self::module."-cron.php?event={$type}";
            $taskId = $this->findJob($urlBase);

            if($taskId) {
                $this->saveSetting($type, $taskId, 'cron');
            }
        }

        if($taskId) {
            $resp = $this->CronApi('jobs/'.$taskId);



            if($resp['success']) {
                $task = $resp['result']['jobDetails'];

                if(!(int)$task['enabled']) {
                    return [
                        'inputVal'=>'N',
                        'job'=>$task
                    ];
                }

                $jobShedule = $this->jobScheduleArrToString($task['schedule']);
                switch ($jobShedule['type'] ) {
                    case 'minutes':
                        return [
                            'inputVal'=>'minutes',
                            'selectVal'=>$this->regularsStr[ $jobShedule['val'] ],
                            'job'=>$task,
                        ];

                    case 'everyday':
                        return [
                            'inputVal'=>'everyday',
                            'hoursVal'=>  $task['schedule']['hours'],
                            'minutesVal'=> $task['schedule']['minutes'],
                            'job'=>$task,
                        ];

                }
            }
        }

        return [
            'inputVal'=>'N'
        ];

    }

    public function SaveCronJobs($jobs){
        if(!is_array($jobs)) {
            $jobs = [];
        }


        $schema = $this->GetJobsTasks();
        foreach ($schema as $type=>$config) {

            if(isset($jobs[$type])) {
                $post = $jobs[$type];

                $status = isset($post['status']) ? $post['status'] : 'N';
                $success = false;


                $shedule = null;
                switch ($status) {
                    case 'minutes':
                        $valPost = $post['minutes'];

                        if(isset(array_flip($this->regularsStr)[$valPost])) {
                            $val = array_flip($this->regularsStr)[$valPost];


                            $shedule = $this->jobScheduleStringToArray($val);
                            $success = true;
                        }


                        break;
                    case 'everyday':


                        $minutes = isset($post['everyday']['min']) ? $post['everyday']['min'] : false;
                        $hour = isset($post['everyday']['hour']) ? $post['everyday']['hour'] : false;

                        if($hour && $minutes) {
                            $shedule = [
                                'minutes'=>[(int)$minutes],
                                'hours'=>[(int)$hour],
                                'wdays'=>[-1],
                                'mdays'=>[-1],
                                'months'=>[-1]
                            ];


                            $success = true;
                        }
                        break;
                    default:
                        $success = false;
                }
                if($success) {
                    $this->createJob($type, $shedule);
                } else {
                    $this->disableCronJob($type);
                }

            } else {
                $this->disableCronJob($type);
            }

        }
    }


    public function CheckCronJobs() {

        //
        // $res = $this->CronApi('/jobs');

        // $res = $this->createJob();
        // print_r($res);
        //$this->saveSetting('test', 'saved', 'cron');

        //print_r($this->getSetting('test', null, 'cron'));
    }



    public function disableCronJob($type){

        $taskId = $this->getSetting($type, null, 'cron');


        if(!$taskId) {
            $urlBase = HTTPS_CATALOG . "system/cron/".self::module."-cron.php?event={$type}";
            $taskId = $this->findJob($urlBase);

            if($taskId) {
                $this->saveSetting($type, $taskId, 'cron');
            }
        }

        if($taskId) {
            $res = $this->CronApi('/jobs/'.$taskId, [ 'job'=>[ 'enabled'=>0] ], 'PATCH');
        }
    }
    public function findJob($url){
        $list = $this->CronApi('/jobs');

        if(!$list['success']) return false;
        $list = $list['result']['jobs'];

        foreach ($list as $item) {
            if(strpos($item['url'], $url) !== false) {
                return  $item['jobId'];
            }
        }

        return false;
    }


    public $regularsStr = [
        '0,5,10,15,20,25,30,35,40,45,50,55&-1&-1&-1&-1'=>5,
        '0,10,20,30,40,50&-1&-1&-1&-1'=>10,
        '0,15,30,45&-1&-1&-1&-1'=>15,
        '0,30&-1&-1&-1&-1'=>30,
        '0&-1&-1&-1&-1'=>60
    ];

    public function jobScheduleStringToArray($scheduleStr){
        $schema = ['minutes','hours', 'wdays', 'mdays', 'months'];
        $testArr = explode('&', $scheduleStr);
        $res = [];

        foreach ($schema as $key=>$test) {
            $res[  $test ] = explode(',', $testArr[$key]);
        }

        return $res;

    }
    public function jobScheduleArrToString($schedule){

        $str = [];
        $schema = ['minutes','hours', 'wdays', 'mdays', 'months'];
        foreach ($schema as $type) {
            $str[] = implode(',', $schedule[$type]);
        }

        $code = implode('&', $str);
        if( isset($this->regularsStr[$code]) ) {
            return [
                'type'=>'minutes',
                'val'=>$code
            ];
        } else {
            return [
                'type'=>'everyday',
                'val'=>$code
            ];
        }
    }

    public function createJob($type, $time){
        $urlBase = HTTPS_CATALOG . "system/cron/".self::module."-cron.php?event={$type}";
        $urlSecurity = $urlBase. "&token=". MapleHelper::ExternalStorageGet('cron_web_token', 'fb243070a8b2bada4779fd2ddc7686b03377610b');
        // Был ли ранее создан такой процесс

        $jobId = $this->findJob($urlBase);

        $time['timezone'] = 'Europe/Moscow';
        $job['job']=[
            'title'=>'[M1SHOP:'.$type.'] '.HTTPS_CATALOG,
            'url'=>$urlSecurity,
            'enabled'=>'true',
            'saveResponses'=>true,
            'schedule'=>$time
        ];

        if($jobId) {

            $res = $this->CronApi('/jobs/'.$jobId, $job, 'PATCH');
            $this->saveSetting($type, $jobId, 'cron');

        } else {
            $res = $this->CronApi('jobs', $job, 'PUT');
            if($res['success'] && isset($res['result']['jobId'])) {
                $this->saveSetting($type,  $res['result']['jobId'], 'cron');
                return  $res['result']['jobId'];
            }

        }

        return false;
    }
}
