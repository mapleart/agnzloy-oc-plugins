<?php

// Heading Goes here:
$_['heading_title']          = 'Интеграция с M1';
$_['m1shop_title']            = 'Интеграция с M1';

// Text
$_['text_module']                = 'Модули';
$_['text_success']               = 'Настройки успешно сохранены';
$_['text_notice']                = 'Внимание! Модель товара должены совпадать с id в выгрузке';
$_['m1shop_base_settings']    = 'Настройки соединения';
$_['m1shop_extended_settings']    = 'Расширенные настройки';

$_['m1shop_url']              = 'Адрес API';
$_['m1shop_apigeo']           = 'Гео зона для api';


// Errors
$_['error_permission']           = 'У вас недостаточно прав на изменение настроек модуля';

