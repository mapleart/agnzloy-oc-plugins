<?php


// Heading Goes here:
$_['heading_title'] = 'Integration with M1';
$_['m1_title'] = 'Integration with M1';

// Text
$_['text_module'] = 'Modules';
$_['text_success'] = 'Settings saved successfully';
$_['text_notice'] = 'Attention! The product model in the  must match the id in the upload';
$_['coatl_base_settings'] = 'Connection Settings';
$_['m1_extended_settings'] = 'Advanced Settings';

$_['m1shop_url'] = 'Spatial address';
$_['m1shop_apigeo'] = 'Geo zone for api';


// Errors
$_['error_permission'] = 'You don\'t have enough rights to change the module settings';