<?php
require_once DIR_SYSTEM.'MapleHelper.php';
class ControllerModuleM1shop extends Controller
{
    /**
     * Create order on event 2
     *
     * @param int $order_id order identificator
     *
     * @return void
     */
    public function order_create($parameter1, $parameter2 = null)
    {

        $this->load->model('checkout/order');
        $this->load->model('account/order');


        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('m1shop');
        $status = isset($settings['m1shop_status']) ? $settings['m1shop_status'] : null;
        $replaceModel = isset($settings['m1shop_replace_model']) ? $settings['m1shop_replace_model'] : null;
        $keyGeo = isset($settings['m1shop_apigeo']) ? $settings['m1shop_apigeo'] : null;


        $WEBMASTER_API = isset($settings['m1shop_webmasterapi']) ? $settings['m1shop_webmasterapi'] : null;
        $WEBMASTER_ID = isset($settings['m1shop_webmasterid']) ? $settings['m1shop_webmasterid'] : null;

        $this->log->write('create order_start');

        if(!$status || !$WEBMASTER_API || !$WEBMASTER_ID || !$replaceModel || !$keyGeo) return;



        if($parameter2 != null)
            $order_id = $parameter2;
        else
            $order_id = $parameter1;

        $data = $this->model_checkout_order->getOrder($order_id);

        $data['products'] = $this->model_account_order->getOrderProducts($order_id);
        $m1Ids = [];
        foreach($data['products'] as $key => $product) {

            $model = $product['model'];
            $pos = strripos($model, $replaceModel);


            if ($pos !== false) {
                $id = (int)str_replace($replaceModel, '', $model);
                if ($id) {
                    $m1Ids[] = $id;
                }
            }

        }

        // Проверяем на существование заказа в панели
        if(!count($m1Ids)){
            return;
        }

        $this->load->model('m1shop/m1shop');
        $oldOrder = $this->model_m1shop_m1shop->getOrderOld('m1shop', $order_id);

        if($oldOrder) {
            return false;
        }

        $phone =  isset($data['telephone']) ? $data['telephone'] : '';

        if(!$phone) {
            return;
        }


        $name = '';
        if($data['firstname'] || $data['lastname']) {
            $name = $data['firstname'].' '. $data['lastname'];
        }

        if(!$name) {
            $name = 'Client #' . $order_id;
        }


        $product_id = $m1Ids[0];
        $url = 'http://m1.top/send_order/';
        $dataApi = [
            'ref' => $WEBMASTER_ID,
            'api_key' => $WEBMASTER_API,
            'product_id' => $product_id,
            'phone' =>  isset($data['telephone']) ? $data['telephone'] : '',
            'name' => $name,
            'ip' => MapleHelper::getRealIpAddr()
        ];


        $data['langCode'] = strtolower($keyGeo);


        $process = curl_init();
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)");
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($process, CURLOPT_TIMEOUT, 20);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($process, CURLOPT_POST, true);
        curl_setopt($process, CURLOPT_POSTFIELDS, $dataApi);
        curl_setopt($process, CURLOPT_URL, $url);

        $return = curl_exec($process);
        $data = json_decode($return, true);

        if($data['result'] == 'ok') {
            $id = $data['id'];
            $this->model_m1shop_m1shop->SyncOrder($order_id, $id, 'm1shop');
        } else if($data['message'] == 'Duplicate order') {
            $this->model_m1shop_m1shop->SyncOrder($order_id, 99999999999, 'm1shop');

        }

        $this->log->write('create order');
        $this->log->write(print_r($dataApi ,true));
        $this->log->write($return);
    }
}
