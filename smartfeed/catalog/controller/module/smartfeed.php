<?php

class ControllerModuleSmartfeed extends Controller
{
    /**
     * Create order on event
     *
     * @param int $order_id order identificator
     *
     * @return void
     */
    public function index()
    {

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $this->load->model('tool/image');
        $this->load->model('setting/setting');
        $this->load->model('catalog/product');
        $settings = $this->model_setting_setting->getSetting('smartfeed');


        $storeId = $this->config->get('config_store_id');
        $storeName = $this->config->get('config_name');;
        $storeLogo = $this->getStoreLogo();
        $storeLink = $this->url->link('common/home');
        $config = $this->getStoreFeedConfig();
        $configDefault = isset($settings['smartfeed_stores'][ 0 ]) ? $settings['smartfeed_stores'][0] : [];



        /// Цена доставки
        $shipingPrice = null;
        if(isset($config['delivery_cost'])) {
            $shipingPrice = $config['delivery_cost'];
        } else if( isset($configDefault['delivery_cost']) ) {
            $shipingPrice = $configDefault['delivery_cost'];
        }


        /// Исключить товары
        ///
        $excluded_products = [];
        if(isset($config['excluded_products']) && is_array($config['excluded_products']) && count($config['excluded_products']) > 0) {
            $excluded_products = $config['excluded_products'];
        } else if( isset($configDefault['excluded_products']) && $configDefault['excluded_products'] && count($configDefault['excluded_products']) > 0 ) {
            $excluded_products = $configDefault['excluded_products'];
        }
        /// Исключить Категории
        ///
        $excluded_categories = [];
        if(isset($config['excluded_categories']) && is_array($config['excluded_categories']) && count($config['excluded_categories']) > 0) {
            $excluded_categories = $config['excluded_categories'];
        } else if( isset($configDefault['excluded_categories']) && $configDefault($config['excluded_categories']) && count($configDefault['excluded_categories']) > 0 ) {
            $excluded_categories = $configDefault['excluded_categories'];
        }

        $this->load->model('localisation/country');
        $country = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));
        if($country) {
            $countryCode = $country['iso_code_2'];
        } else {
            $countryCode = $country['iso_code_2'];
        }

        $this->load->model('smartfeed/smartfeed');
        $productIds = $this->model_smartfeed_smartfeed->getProducts();

        $products=[];
        foreach ($productIds as $result) {
            $products[$result['product_id']] = $this->model_catalog_product->getProduct($result['product_id']);
        }


        header("Access-Control-Allow-Origin: *");
       header("Content-Type: text/xml; charset=UTF-8");

       echo '<?xml version="1.0" encoding="utf-8"?>';
       if(!isset($_GET['test'])) {
           echo '<?xml-stylesheet type="text/xsl" href="/index.php?route=module/smartfeed/productspreview"?>';
       }
        ?>

        <rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
            <channel>
                <title><?= $storeName ?></title>
                <link><?= $storeLink ?></link>

                <?php

                foreach ($products as $product) {

                    if( in_array($product['product_id'], $excluded_products)){
                        continue;
                    }



                    $success = true;

                    foreach ($this->model_catalog_product->getCategories($product['product_id']) as $pCat) {


                        if(in_array($pCat['category_id'], $excluded_categories)) {
                            $success = false;
                        }
                    }


                    if(!$success) continue;

                    if ($product['image']) {
                        $image = $this->model_tool_image->resize($product['image'], 500, 500);
                        $imageAdd = $this->model_tool_image->resize($product['image'], 800, 800);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', 500, 500);
                        $imageAdd = $this->model_tool_image->resize('placeholder.png', 800, 800);
                    }


                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price =   str_replace('.', '',number_format($product['price'], 0, $this->language->get('decimal_point'), $this->language->get('thousand_point'))  .' '. $this->config->get('config_currency'));
                    } else {
                        $price = false;
                    }

                    if ((float)$product['special']) {
                        $special = str_replace('.', '', number_format($product['special'], 0, $this->language->get('decimal_point'), $this->language->get('thousand_point')) .' '. $this->config->get('config_currency'));
                    } else {
                        $special = false;
                    }

                    $description = utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..';


                ?>
                <item>
                    <title><?= htmlspecialchars($product['name'], ENT_XML1, 'utf-8' );?> </title>
                    <link><?=  $this->url->link('product/product', 'product_id=' . $product['product_id']) ?></link>
                    <description><![CDATA[ <?= $description; ?> ]]></description>
                    <g:price><?= $special ? $special : $price  ?></g:price>
                    <g:availability>in stock</g:availability>
                    <g:condition>new</g:condition>
                    <g:id><?= $product['product_id']?></g:id>
                    <g:image_link><?= $image ?></g:image_link>

                    <g:additional_image_link><?= $imageAdd ?></g:additional_image_link>
                    <g:shipping>
                        <g:country><?= strtoupper($countryCode) ?></g:country>
                        <g:price><?= $shipingPrice;  ?></g:price>
                    </g:shipping>
                    <g:brand><?= $storeName ?></g:brand>

                    <g:identifier_exists>no</g:identifier_exists>
                </item>

                    <?php } ?>
            </channel>
        </rss>
        <?php
        die();

    }


    public function getStoreFeedConfig(){

        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('smartfeed');
        $storeId = $this->config->get('config_store_id');

        return isset($settings['smartfeed_stores'][ $storeId ]) ? $settings['smartfeed_stores'][$storeId] : [];

    }
    public function getStoreLogo(){
        $this->load->model('tool/image');

        if ( is_file(DIR_IMAGE . $this->config->get('config_logo') )) {
            return $this->model_tool_image->resize( $this->config->get('config_logo') , 100, 100);
        } else {
            return $this->model_tool_image->resize('no_image.png', 100, 100);
        }

    }
    public function productspreview(){


        $storeId = $this->config->get('config_store_id');
        $storeName = $this->config->get('config_name');;
        $storeLogo = $this->getStoreLogo();
        $storeLink = $this->url->link('common/home');
        $config = $this->getStoreFeedConfig();


        header("Access-Control-Allow-Origin: *");
        header("Content-Type: text/xml; charset=UTF-8");
        echo '<?xml version="1.0"?>';
        ?>
        <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

            <xsl:output method="html"/>

            <!-- ======================================================================= -->
            <xsl:template match="channel">
                <!-- ======================================================================= -->
                <html>
                <head>
                    <title><xsl:value-of select="title"/></title>
                    <meta name="MSSmartTagsPreventParsing" content="TRUE" />
                    <meta http-equiv="imagetoolbar" content="no" />
                    <meta name="viewport" content="initial-scale=0.5" />
                    <link rel="stylesheet" type="text/css" href="/catalog/view/smartfeed/style.css" />

                    <script type="text/javascript">
                        function doLoad() {
                            if (navigator.userAgent.toLowerCase().indexOf("msie") > -1) return;
                            for (var i = 1; i &lt;= <xsl:value-of select="count(item)" />; i++) {
                                unescapeHTML('item_' + i);
                            }
                        }

                        function unescapeHTML(id) {
                            var lt = new RegExp("&amp;lt;", "g");
                            var gt = new RegExp("&amp;gt;", "g");
                            var amp = new RegExp("&amp;amp;", "g");

                            var obj = document.getElementById(id);
                            var s = obj.innerHTML;
                            obj.innerHTML = s.replace(lt, "&lt;").replace(gt, "&gt;").replace(amp, "&amp;");
                        }
                    </script>
                </head>
                <body onload="doLoad()">
                <div>

                    <div class="logo"><a href="/"><img src="<?= $storeLogo ?>" width="30" height="29" alt="XML Generator" align="top" /></a></div>
                    <div class="footer oCenter">
                        Feed created with <a href="<?= $storeLink ?>"><?= $storeName ?></a>
                    </div>
                    <div class="main oCenter">
                        <h1><a href="{link}"><xsl:value-of select="title"/></a></h1>
                        <xsl:if test="description">
                            <p><xsl:value-of select="description"/></p>
                        </xsl:if>

                    </div>

                    <div class="main">
                        <xsl:apply-templates select="item"/>
                    </div>

                    <div class="footer oCenter">
                        Feed created with <a href="<?= $storeLink ?>"><?= $storeName ?></a>
                    </div>

                </div>

                <!-- scripts -->
                <?= isset($config['scripts']) ? htmlspecialchars_decode($config['scripts']) : ''; ?>
                </body>
                </html>
            </xsl:template>

            <!-- ======================================================================= -->
            <xsl:template match="item">
                <!-- ======================================================================= -->
                <div class="itemline productview">
                    <xsl:if test="title">
                        <h2><a href="{link}" target="_blank"><xsl:value-of select="title"/></a></h2>
                    </xsl:if>
                    <div id="item_{position()}">
                        <div>
                            <xsl:choose>
                                <xsl:when test="*[name()='g:sale_price']">
                                    <strike><xsl:value-of select="*[name()='g:price']"/></strike>
                                    <xsl:value-of select="*[name()='g:sale_price']"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="*[name()='g:price']"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                        <div class="desc">
                            <xsl:if test="*[name()='g:image_link']">
                                <img class="preview">
                                <xsl:attribute name="src">
                                    <xsl:value-of select="*[name()='g:image_link']"/>
                                </xsl:attribute>
                                </img>
                            </xsl:if>
                            <xsl:if test="picture">
                                <img class="preview">
                                <xsl:attribute name="src">
                                    <xsl:value-of select="picture"/>
                                </xsl:attribute>
                                </img>
                            </xsl:if>
                            <xsl:if test="description">
                                <xsl:value-of select="description" disable-output-escaping="yes"/>
                            </xsl:if>
                        </div>
                    </div>
                </div>
            </xsl:template>

        </xsl:stylesheet>
<?php
    }
}
