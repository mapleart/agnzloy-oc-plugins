<?php

class ModelSmartfeedSmartfeed extends Model {

    public function getProducts() {
        $sql = "SELECT p.product_id
            FROM " . DB_PREFIX . "product p
        
        LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
         LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) 
         
         
        WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' 
                AND p.status = '1'
                AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'
                ";

        $sql .= " GROUP BY p.product_id";


        $product_data = array();

        $query = $this->db->query($sql);

        return  $query->rows;
    }

}
