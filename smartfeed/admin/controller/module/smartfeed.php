<?php

class ControllerModuleSmartfeed extends Controller
{

    public function __construct($registry)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        parent::__construct($registry);
        $this->getModuleData();
    }

    private $status = false;
    private $_error = array();
    private $_warning = array();

    private $scheme = [
        'createFastOrder'=>[
            'title'=>'Создание заказа в 1 клик',
            'code'=>'createFastOrder',
            'type'=>'custom'
        ],
        'createOrder'=>[
            'title'=>'Создание заказа',
            'code'=>'createOrder',
            'type'=>'custom'
        ]
    ];

    /**
     * Выполняется при активации
     * @return void
     */
    public function install() {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('smartfeed', array('smartfeed_status' => 1));
    }


    /**
     * Выполняется при деактивации
     * @return void
     */
    public function uninstall() {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('smartfeed', array('smartfeed_status' => 0));
    }

    public function getBreadcrumbs(){
        $res[] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link(
                'common/home',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => false
        );

        $res[] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link(
                'extension/module',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        $res[] = array(
            'text'      => $this->language->get('smartfeed_title'),
            'href'      => $this->url->link('module/smartfeed', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        
        return $res;
    }

    /**
     * Главная страница
     *
     * @return void
     */
    public function index()
    {
        $this->load->model('setting/setting');
        $this->load->model('extension/module');
        $this->load->language('module/smartfeed');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('/admin/view/stylesheet/smartfeed.css');

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {


            $redirect = $this->url->link('module/smartfeed', 'token=' . $this->session->data['token'], 'SSL');


            if ($this->request->post && isset($this->request->post['form'])) {
                $form = $this->request->post['form'];
                if ($form == 'import'){
                    $this->import();

                    $this->response->redirect($redirect);

                }
            }


            $save = $this->request->post;

            $this->model_setting_setting->editSetting('smartfeed', $save);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($redirect);
        }

        $this->load->model('extension/extension');
        $_data = &$data;



        $_data['smartfeed_errors'] = array();
        $_data['saved_settings'] = $this->model_setting_setting->getSetting('smartfeed');

        if (isset($this->error['warning'])) {
            $_data['error_warning'] = $this->error['warning'];
        } else {
            $_data['error_warning'] = '';
        }

        $_data['breadcrumbs'] = $this->getBreadcrumbs();



        $_data['action'] = $this->url->link('module/smartfeed', 'token=' . $this->session->data['token'], 'SSL');

        $_data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');



        $this->load->model('design/layout');
        $_data['token'] = $this->session->data['token'];
        $_data['layouts'] = $this->model_design_layout->getLayouts();
        $_data['header'] = $this->load->controller('common/header');
        $_data['column_left'] = $this->load->controller('common/column_left');
        $_data['footer'] = $this->load->controller('common/footer');

        if (isset($this->session->data['success'])) {
            $data['alert_notice'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['alert_notice'] = '';
        }
        if (isset($this->session->data['error'])) {
            $data['error_notice'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_notice'] = '';
        }



        $data['scheme'] = $this->scheme;
        $data = array_merge($_data, $this->getTemplateData());

        $this->response->setOutput(
            $this->load->view('module/smartfeed.tpl', $_data)
        );
    }

    /**
     * Создание переменных для модуля
     * @return false
     */
    public function getModuleData() {

        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('smartfeed');

        $this->status = isset($settings['status']) ? $settings['status'] : null;
    }

    public function getTemplateData(){
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('smartfeed'); // Настройки плагина


        $result = [
            'lifetime'=> isset($settings['smartfeed_lifetime']) ? (int)$settings['smartfeed_lifetime'] : 0,
        ];


        /////
        ///
        $this->load->model('catalog/category');
        if (isset($this->request->post['smartfeed_categories_excluded'])) {
            $categories = $this->request->post['smartfeed_categories_excluded'];
        } elseif (isset($settings['smartfeed_categories_excluded'])) {
            $categories = $settings['smartfeed_categories_excluded'];
        } else {
            $categories = array();
        }

        $result['categories_excluded'] = array();

        foreach ($categories as $category_id) {
            $category_info = $this->model_catalog_category->getCategory($category_id);

            if ($category_info) {
                $result['categories_excluded'][] = array(
                    'category_id' => $category_info['category_id'],
                    'name' => ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name']
                );
            }
        }

        /////
        if (isset($this->request->post['smartfeed_products_excluded'])) {
            $products = $this->request->post['smartfeed_products_excluded'];
        } elseif (isset($settings['smartfeed_products_excluded'])) {
            $products = $settings['smartfeed_products_excluded'];
        } else {
            $products = array();
        }


        $result['products_excluded'] = array();

        foreach ($products as $product_id) {
            $related_info = $this->model_catalog_product->getProduct($product_id);

            if ($related_info) {
                $result['products_excluded'][] = array(
                    'product_id' => $related_info['product_id'],
                    'name'       => $related_info['name']
                );
            }
        }

        ////
        ////
        $stories = array_merge(
            [
                [
                    'store_id' => 0,
                    'name'     => $this->config->get('config_name') . $this->language->get('text_default'),
                    'url'      => HTTP_CATALOG,
                    'edit'     => $this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL')
                ]
            ], $this->model_setting_store->getStores());



        foreach ($stories as $store) {
            $savedStore = isset($settings['smartfeed_stores'][$store['store_id']]) ? $settings['smartfeed_stores'][$store['store_id']] : [];


            $storeData = array(
                'store_id' => $store['store_id'],
                'name'     => $store['name'],
                'url'      => $store['url'],
                'urlText'      => trim(str_replace(['https://', 'http://'], ['', ''], $store['url']), '/'),
                'urlFeed'      => trim($store['url'], '/')  . '/index.php?route=module/smartfeed',
                'exclude_categories_entity'=>[],
                'exclude_products_entity'=>[],
                'excluded_products'=>[],
                'excluded_categories'=>[],
                'delivery_cost'=>isset($savedStore['delivery_cost']) ? $savedStore['delivery_cost'] : '',
                'scripts'=>isset($savedStore['scripts']) ? $savedStore['scripts'] : '',
            );



            ///
            /// Категории в исключении
            if (isset($this->request->post['smartfeed_stores'])) {
                $excludeCategories = $this->request->post['smartfeed_stores'];
            } elseif (isset( $savedStore['excluded_categories'] )) {
                $excludeCategories = $savedStore['excluded_categories'];
            } else {
                $excludeCategories = array();
            }

            $storeData['excluded_categories'] = $excludeCategories;

            foreach ($excludeCategories as $category_id) {
                $category_info = $this->model_catalog_category->getCategory($category_id);

                if ($category_info) {
                    $storeData['exclude_categories_entity'][] = array(
                        'category_id' => $category_info['category_id'],
                        'name' => ($category_info['path']) ? $category_info['path'] . ' &gt; ' . $category_info['name'] : $category_info['name']
                    );
                }
            }
            ////
            ///

            if (isset($this->request->post['smartfeed_stores'])) {
                $products = $this->request->post['smartfeed_stores'];
            } elseif ( isset($savedStore['excluded_products'])) {
                $products = $savedStore['excluded_products'];
            } else {
                $products = array();
            }

            $storeData['excluded_products'] = $products;

            foreach ($products as $product_id) {
                $related_info = $this->model_catalog_product->getProduct($product_id);

                if ($related_info) {
                    $storeData['exclude_products_entity'][] = array(
                        'product_id' => $related_info['product_id'],
                        'name'       => $related_info['name']
                    );
                }
            }


            $result['stores'][$store['store_id']] = $storeData;
        }




        return $result;

    }

    /**
     * Validate
     *
     * @return bool
     */
    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/smartfeed')) {
            $this->_error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->_error) {
            return true;
        } else {
            return false;
        }
    }

    private function sendMail( $subject, $html, $file = '')
    {
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->notify_email);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);

        if($file) {
            $mail->addAttachment($file);   // I took this from the phpmailer example on github but I'm not sure if I have it right.
        }

        $mail->setHtml($html);
        $mail->send();
    }
}
