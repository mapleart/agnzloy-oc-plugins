<?php

// Heading Goes here:
$_['heading_title']          = 'Smart Feed';
$_['targets_title']            = 'Smart Feed';

// Text
$_['text_module']                = 'Модули';
$_['text_success']               = 'Настройки успешно сохранены';

// Errors
$_['error_permission']           = 'У вас недостаточно прав на изменение настроек модуля';

