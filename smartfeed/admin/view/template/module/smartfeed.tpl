<?php echo $header; ?>
<?php echo $column_left;?>

<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-smartfeed" data-toggle="tooltip" title="Сохранить" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <h1>Smartfeed</h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <!-- -->
        <?php if ($error_warning) : ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        </div>
        <?php endif; ?>

        <!-- -->
        <?php if ($alert_notice) : ?>
        <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert_notice; ?>
        </div>
        <?php endif; ?>

        <!-- -->
        <?php if ($error_notice) : ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $error_notice; ?>
        </div>
        <?php endif; ?>



        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="panel-header">Как работает модуль?</h3>

                <div class="alert alert-info">
                    <p>Модуль генерирует feed</p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">

                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-smartfeed">
                    <input type="hidden" name="smartfeed_status" value="1">


                    <h3 class="panel-header">Общие правила для всех магазинов</h3>

                    <div class="form-group row">
                        <label class="col-sm-2 control-label" for="input-lifetime">Время жизни кеша</label>
                        <div class="col-sm-10">
                            <input type="number" name="smartfeed_lifetime" value="<?= (int)$lifetime ?>" placeholder="Сколько хранить кеш в минутах" id="input-lifetime" class="form-control" />

                        </div>
                    </div>

                    <br>
                    <br>

                    <h3 class="panel-header">Настройки для магазинов</h3>
                    <div class="alert alert-info">Если указать отдельно для магазина, значение по умолчанию будет заменено </div>

                    <table class="table">
                    <thead>
                    <tr>
                        <th style="width: 30%;">Название магазина</th>
                        <th>Исключить категории</th>
                        <th>Исключить товары</th>
                        <th>Цена доставки</th>
                        <th>Счетчики (ГА, Яндекс метрика и тд)</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach($stores as $store) { ?>
                        <tr>
                            <td>
                                <a target="_blank" href="<?= $store['url']; ?>"><?= $store['name']; ?></a> | <a href="<?= $store['urlFeed']; ?>" target="_blank">feed</a> <br>

                                <strong style="font-size: 16px;"><?= $store['urlText']; ?></strong>
                            </td>
                            <td>
                                <input type="text" name="category" value="" placeholder="Исключить категории" id="input-exclude-category<?= $store['store_id']; ?>" class="form-control js-autocomplete-category" data-store-id="<?= $store['store_id']; ?>" />
                                <div id="store-exclude-category<?= $store['store_id']; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
                                    <?php foreach ($store['exclude_categories_entity'] as $category) { ?>
                                    <div id="store-exclude-category<?= $store['store_id']; ?>_<?php echo $category['category_id']; ?>"><i class="fa fa-minus-circle js-delete-exclude-category"></i> <?php echo $category['name']; ?>
                                        <input type="hidden" name="smartfeed_stores[<?= $store['store_id']; ?>][excluded_categories][]" value="<?php echo $category['category_id']; ?>" />
                                    </div>
                                    <?php } ?>
                                </div>
                            </td>
                            <td>
                                <input type="text" name="related" value="" placeholder="Исключить товар" id="input-exclude-product<?= $store['store_id']; ?>" class="form-control js-autocomplete-product"  data-store-id="<?= $store['store_id']; ?>" />
                                <div id="store-exclude-product<?= $store['store_id']; ?>" class="well well-sm" style="height: 150px; overflow: auto;">
                                    <?php foreach ($store['exclude_products_entity']  as $product) { ?>
                                    <div id="store-exclude-product<?= $store['store_id']; ?>_<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle js-delete-exclude-product"></i> <?php echo $product['name']; ?>
                                        <input type="hidden" name="smartfeed_stores[<?= $store['store_id']; ?>][excluded_products][]" value="<?php echo $product['product_id']; ?>" />
                                    </div>
                                    <?php } ?>
                                </div>
                            </td>
                            <td>
                                <input type="number" name="smartfeed_stores[<?= $store['store_id']; ?>][delivery_cost]" value="<?= $store['delivery_cost'] ?>" placeholder="Стоимость доставки" class="form-control" />
                            </td>

                            <td>
                                <textarea name="smartfeed_stores[<?= $store['store_id']; ?>][scripts]" class="form-control" rows="8"><?= $store['scripts'] ?></textarea>
                            </td>
                        </tr>
                    <?php }  ?>
                    </tbody>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript"><!--


        // Excluded Category
        $('input[name=\'excluded-category\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item['name'],
                                value: item['category_id']
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                $('input[name=\'category\']').val('');

                $('#excluded-categories' + item['value']).remove();

                $('#excluded-categories').append('<div id="excluded-categories' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="smartfeed_categories_excluded[]" value="' + item['value'] + '" /></div>');
            }
        });

        $('#excluded-categories').delegate('.fa-minus-circle', 'click', function() {
            $(this).parent().remove();
        });

        // Excluded products
        $('input[name=\'excluded-products\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                $('input[name=\'excluded-products\']').val('');

                $('#product-excluded' + item['value']).remove();

                $('#product-excluded').append('<div id="product-excluded' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="smartfeed_products_excluded[]" value="' + item['value'] + '" /></div>');
            }
        });

        $('#product-excluded').delegate('.fa-minus-circle', 'click', function() {
            $(this).parent().remove();
        });



        // Category
        $('.js-autocomplete-category').each( function () {
            var $el = $(this),
                storeId = $el.data('store-id')

            $el.autocomplete({
                'source': function (request, response) {
                    $.ajax({
                        url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                        dataType: 'json',
                        success: function (json) {
                            response($.map(json, function (item) {
                                return {
                                    label: item['name'],
                                    value: item['category_id']
                                }
                            }));
                        }
                    });
                },
                'select': function (item) {
                    $el.val('');
                    $('#store-exclude-category' + storeId + '_' + item['value']).remove();
                    $('#store-exclude-category' + storeId).append('<div id="store-exclude-category' + storeId + '_' + item['value'] + '"><i class="fa fa-minus-circle js-delete-exclude-category"></i> ' + item['label'] + '<input type="hidden" name="smartfeed_stores[' + storeId + '][excluded_categories][]" value="' + item['value'] + '" /></div>');
                }
            });
        });
        $(document).on('click', '.js-delete-exclude-category', function() {
            $(this).parent().remove();
        });


        // Related
        $('.js-autocomplete-product').each( function () {
            var $el = $(this),
                storeId = $el.data('store-id')

            $el.autocomplete({
                'source': function(request, response) {
                    $.ajax({
                        url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                        dataType: 'json',
                        success: function(json) {
                            response($.map(json, function(item) {
                                return {
                                    label: item['name'],
                                    value: item['product_id']
                                }
                            }));
                        }
                    });
                },
                'select': function(item) {
                    $el.val('');
                    $('#store-exclude-product' + storeId + '_' + item['value']).remove();
                    $('#store-exclude-product' + storeId).append('<div id="store-exclude-category' + storeId + '_' + item['value'] + '"><i class="fa fa-minus-circle js-delete-exclude-category"></i> ' + item['label'] + '<input type="hidden" name="smartfeed_stores[' + storeId + '][excluded_products][]" value="' + item['value'] + '" /></div>');
                }
            });
        });

        $(document).on('click', '.js-delete-exclude-product', function() {
            $(this).parent().remove();
        });


        //--></script>

<?php echo $footer; ?>
