<?php

// Heading Goes here:
$_['heading_title']          = 'Кастомные цели для аналитики';
$_['targets_title']            = 'Кастомные цели для аналитики';

// Text
$_['text_module']                = 'Модули';
$_['text_success']               = 'Настройки успешно сохранены';

// Errors
$_['error_permission']           = 'У вас недостаточно прав на изменение настроек модуля';
