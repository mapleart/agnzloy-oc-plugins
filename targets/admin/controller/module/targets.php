<?php

class ControllerModuleTargets extends Controller
{

    public function __construct($registry)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        parent::__construct($registry);
        $this->getModuleData();
    }

    private $status = false;
    private $_error = array();
    private $_warning = array();

    private $scheme = [
        'createFastOrder'=>[
            'title'=>'Создание заказа в 1 клик',
            'code'=>'createFastOrder',
            'type'=>'custom'
        ],
        'createOrder'=>[
            'title'=>'Создание заказа',
            'code'=>'createOrder',
            'type'=>'custom'
        ]
    ];

    /**
     * Install method
     *
     * @return void
     */
    public function install()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('targets_status', array('targets_status' => 1));
    }
    /**
     * Uninstall method
     *
     * @return void
     */
    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('targets', array('targets_status' => 0));

    }

    /**
     * Главная страница
     *
     * @return void
     */
    public function index()
    {
        $this->load->model('setting/setting');
        $this->load->model('extension/module');
        $this->load->language('module/targets');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('/admin/view/stylesheet/targets.css');

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {


            $redirect = $this->url->link(
                'module/targets', 'token=' . $this->session->data['token'],
                'SSL'
            );


            if ($this->request->post && isset($this->request->post['form'])) {
                $form = $this->request->post['form'];
                if ($form == 'import'){
                    $this->import();

                    $this->response->redirect($redirect);

                }
            }

            $this->model_setting_setting->editSetting(
                'targets',
                $this->request->post
            );

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($redirect);
        }

        $this->load->model('extension/extension');
        $_data = &$data;



        $_data['targets_errors'] = array();
        $_data['saved_settings'] = $this->model_setting_setting
            ->getSetting('targets');

        $config_data = array(
            'targets_status'
        );



        foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $_data[$conf] = $this->request->post[$conf];
            } else {
                $_data[$conf] = $this->config->get($conf);
            }
        }

        if (isset($this->error['warning'])) {
            $_data['error_warning'] = $this->error['warning'];
        } else {
            $_data['error_warning'] = '';
        }

        $_data['breadcrumbs'] = array();

        $_data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link(
                'common/home',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => false
        );

        $_data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link(
                'extension/module',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        $_data['breadcrumbs'][] = array(
            'text'      => $this->language->get('targets_title'),
            'href'      => $this->url->link(
                'module/targets',
                'token=' . $this->session->data['token'], 'SSL'
            ),
            'separator' => ' :: '
        );

        $_data['action'] = $this->url->link(
            'module/targets',
            'token=' . $this->session->data['token'], 'SSL'
        );

        $_data['cancel'] = $this->url->link(
            'extension/module',
            'token=' . $this->session->data['token'], 'SSL'
        );

        $_data['modules'] = array();

        if (isset($this->request->post['targets_module'])) {
            $_data['modules'] = $this->request->post['targets_module'];
        } elseif ($this->config->get('targets_module')) {
            $_data['modules'] = $this->config->get('targets_module');
        }

        $this->load->model('design/layout');
        $_data['token'] = $this->session->data['token'];
        $_data['layouts'] = $this->model_design_layout->getLayouts();
        $_data['header'] = $this->load->controller('common/header');
        $_data['column_left'] = $this->load->controller('common/column_left');
        $_data['footer'] = $this->load->controller('common/footer');

        if (isset($this->session->data['success'])) {
            $data['alert_notice'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['alert_notice'] = '';
        }
        if (isset($this->session->data['error'])) {
            $data['error_notice'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error_notice'] = '';
        }

        $data['scheme'] = $this->scheme;
        $this->response->setOutput(
            $this->load->view('module/targets.tpl', $_data)
        );
    }

    /**
     * Создание переменных для модуля
     * @return false
     */
    public function getModuleData() {

        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('targets');

        $this->status = isset($settings['targets_status']) ? $settings['targets_status'] : null;
    }


    /**
     * Validate
     *
     * @return bool
     */
    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/targets')) {
            $this->_error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->_error) {
            return true;
        } else {
            return false;
        }
    }

    private function sendMail( $subject, $html, $file = '')
    {
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->notify_email);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject($subject);

        if($file) {
            $mail->addAttachment($file);   // I took this from the phpmailer example on github but I'm not sure if I have it right.
        }

        $mail->setHtml($html);
        $mail->send();
    }
}
