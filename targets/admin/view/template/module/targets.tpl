<?php echo $header; ?>
<?php echo $column_left;?>

<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-targets" data-toggle="tooltip" title="Сохранить" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <h1>Targets</h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) : ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        </div>
        <?php endif; ?>


        <?php if ($alert_notice) : ?>
        <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $alert_notice; ?>
        </div>
        <?php endif; ?>
        <?php if ($error_notice) : ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $error_notice; ?>
        </div>
        <?php endif; ?>



        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="panel-header">Как работает модуль?</h3>

                <div class="alert alert-info">
                    <p>Модуль генерирует кастомные события для аналитики и не только. Модуль создает области куда можно инжектить произвольный код.</p>
                    <p>Код вставляется без тега script</p>
                    <p>Обязательно обновите кэш модификаторов! после изменения</p>

                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-body">
                <h3 class="panel-header">Доступные цели</h3>

                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-targets">
                <table class="table">
                    <thead>
                    <tr>
                        <th style="width: 30%;">Название цели</th>
                        <th>Код</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach($scheme as $schemePlace) { ?>
                        <tr>
                            <td><?php echo $schemePlace['title']; ?></td>
                            <td>
                                <textarea class="form-control" rows="5" name="targets[<?php echo $schemePlace['code']; ?>]"><?php if (isset($saved_settings['targets'][ $schemePlace['code'] ])): echo $saved_settings['targets'][ $schemePlace['code']  ]; endif;?></textarea>
                            </td>
                        </tr>
                    <?php }  ?>
                    </tbody>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>

<?php echo $footer; ?>
