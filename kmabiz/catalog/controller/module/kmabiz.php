<?php
require_once DIR_SYSTEM.'MapleHelper.php';

class ControllerModuleKmabiz extends Controller
{
    /**
     * Create order on event 2
     *
     * @param int $order_id order identificator
     *
     * @return void
     */
    public function order_create($parameter1, $parameter2 = null)
    {

        $this->load->model('checkout/order');
        $this->load->model('account/order');


        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('kmabiz');
        $status = isset($settings['kmabiz_status']) ? $settings['kmabiz_status'] : null;
        $replaceModel = isset($settings['kmabiz_replace_model']) ? $settings['kmabiz_replace_model'] : null;
        $keyGeo = isset($settings['kmabiz_apigeo']) ? $settings['kmabiz_apigeo'] : null;


        $WEBMASTER_API = isset($settings['kmabiz_webmasterapi']) ? $settings['kmabiz_webmasterapi'] : null;

        $this->log->write('create order_start');

        if(!$status || !$WEBMASTER_API || !$replaceModel || !$keyGeo) return;



        if($parameter2 != null)
            $order_id = $parameter2;
        else
            $order_id = $parameter1;

        $data = $this->model_checkout_order->getOrder($order_id);

        $data['products'] = $this->model_account_order->getOrderProducts($order_id);
        $kmaIds = [];
        foreach($data['products'] as $key => $product) {

            $model = $product['model'];
            $pos = strripos($model, $replaceModel);


            if ($pos !== false) {
                $id = (int)str_replace($replaceModel, '', $model);
                if ($id) {
                    $kmaIds[] = $id;
                }
            }

        }

        // Проверяем на существование заказа в панели
        if(!count($kmaIds)){
            return;
        }

        $this->load->model('kmabiz/kmabiz');
        $oldOrder = $this->model_kmabiz_kmabiz->getOrderOld('kmabiz', $order_id);

        if($oldOrder) {
            return false;
        }

        $phone =  isset($data['telephone']) ? $data['telephone'] : '';

        if(!$phone) {
            return;
        }



        $product_id = $kmaIds[0];
        ////
        ///
        ///
        ///

        $url = "https://api.kma.biz/channel/create?token={$WEBMASTER_API}&offer_id={$product_id}&source_id=1";
        $channelRes = file_get_contents($url);
        $channel_id = '';
        if($arr = @json_decode($channelRes, true) and is_array($arr)) {


            if(isset($arr['channel'])) {
                $channel_id = $arr['channel'];
            } else {
                return;
            }
        } else {
            return false;
        }
        ////
        ///
        ///
        ///
        ///
        ///

        $name = '';
        if($data['firstname'] || $data['lastname']) {
            $name = $data['firstname'].' '. $data['lastname'];
        }

        if(!$name) {
            $name = 'Client #' . $order_id;
        }

        $url = "https://api.kma.biz/lead/add";
        $dataApi = [
            'channel' => $channel_id,
            'name' => $name,
            'phone' =>  isset($data['telephone']) ? $data['telephone'] : '',
            'ip' => MapleHelper::getRealIpAddr(),
            'country' => $keyGeo,
            'language' =>  substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2),
            'referer' => $_SERVER['HTTP_REFERER'],

        ];


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dataApi));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer '.$WEBMASTER_API,
            'Content-Type: application/x-www-form-urlencoded'
        ]);


        $return = curl_exec($ch);
        $data = json_decode($return, true);

        if($data['message'] == 'ok') {
            $id = $data['id'];
            $this->model_kmabiz_kmabiz->SyncOrder($order_id, $id, 'kmabiz');
        } else if($data['message'] == 'Duplicate order') {
            $this->model_kmabiz_kmabiz->SyncOrder($order_id, 99999999999, 'kmabiz');

        }

        $this->log->write('create order');
        $this->log->write(print_r($dataApi ,true));
        $this->log->write($return);
    }
}
