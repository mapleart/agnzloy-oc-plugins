<?php
// Heading Goes here:
$_['heading_title'] = 'Integration with KMA.BIZ';
$_['kmabiz_title'] = 'Integration with KMA.BIZ';

// Text
$_['text_module'] = 'Modules';
$_['text_success'] = 'Settings saved successfully';
$_['text_notice'] = 'Attention! The product model in the  must match the id in the upload';
$_['coatl_base_settings'] = 'Connection Settings';
$_['kmabiz_extended_settings'] = 'Advanced Settings';

$_['kmabiz_url'] = 'Spatial address';
$_['kmabiz_apigeo'] = 'Geo zone for api';


// Errors
$_['error_permission'] = 'You don\'t have enough rights to change the module settings';