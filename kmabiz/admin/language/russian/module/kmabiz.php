<?php

// Heading Goes here:
$_['heading_title']          = 'Интеграция с KMA.BIZ';
$_['kmabiz_title']            = 'Интеграция с KMA.BIZ';

// Text
$_['text_module']                = 'Модули';
$_['text_success']               = 'Настройки успешно сохранены';
$_['text_notice']                = 'Внимание! Модель товара должены совпадать с id в выгрузке';
$_['kmabiz_base_settings']    = 'Настройки соединения';
$_['kmabiz_extended_settings']    = 'Расширенные настройки';

$_['kmabiz_url']              = 'Адрес API';
$_['kmabiz_apigeo']           = 'Гео зона для api';


// Errors
$_['error_permission']           = 'У вас недостаточно прав на изменение настроек модуля';

