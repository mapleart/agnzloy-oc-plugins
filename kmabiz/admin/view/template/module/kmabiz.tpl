<?php echo $header; ?>
<?php echo $column_left;?>

<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-kmabiz" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>

    </div>
    <div class="container-fluid">
        <?php if ($error_warning) : ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($saved_settings['kmabiz_url'])): ?>
            <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $text_notice; ?>
            </div>
        <?php endif; ?>

        <?php if ($alert_notice) : ?>
            <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $alert_notice; ?>
            </div>
        <?php endif; ?>
        <?php if ($error_notice) : ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo $error_notice; ?>
            </div>
        <?php endif; ?>

        <?php if(isset($check_html) && $check_html) { ?>
            <div>
                <?php echo ($check_html); ?>
            </div>
        <?php } ?>





        <div class="panel panel-default">
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-kmabiz">
                    <input type="hidden" name="kmabiz_status" value="1">

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab">Основные настройки</a></li>
                        <li><a href="#tab-notifications" data-toggle="tab">Уведомления и CRON</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <?= $mainForm; ?>
                        </div>
                        <div class="tab-pane" id="tab-notifications">
                            <?= $notificationsForm; ?>
                        </div>
                    </div>


                </form>
            </div>
        </div>

        <?php if (!empty($new_offers)) : ?>

            <div class="panel panel-default">

                <div class="panel-body">
                    <h3 class="panel-header">Импорт товаров</h3>


                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-import-kmabiz">
                        <input type="hidden" name="form" value="import">

                        <button type="submit" class="btn btn-primary">
                            Импорт
                        </button>
                    </form>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>

<?php echo $footer; ?>
