<h3>Основные настройки</h3>

<?php if($file_data && $parsed_file) { ?>

    <div class="alert alert-success">
        <h5>Файл найден</h5>
        <p>Количество офферов в файле: <?php echo $parsed_file['count_offers']; ?> </p>
        <p>Время жизни файла: <?php echo $parsed_file['live']; ?> мин. </p>
        <p>Когда был создан (по мск): <?php echo $parsed_file['time']; ?> мин. </p>

        <?php if($file_data['countries']) { ?>
            <p>В таблице отсутствуют страны <?php echo implode(',', $file_data['countries'] ); ?> </p>
        <?php } ?>
        <br>

        <a href="<?php echo $linkFileUpdate ?>" target="_blank" class="btn btn-success">Обновить</a>
    </div>

<?php } else { ?>
    <div class="alert alert-danger">
        Файл с офферами не найден
    </div>
<?php } ?>

<div class="form-group">
    <label for="kmabiz_apigeo"> GEO магазина</label><br>
    <select id="kmabiz_apigeo"  name="kmabiz_apigeo" class="form-control" >
        <?php foreach ($countries as $country) { ?>
            <option <?php echo (isset($saved_settings['kmabiz_apigeo']) && $saved_settings['kmabiz_apigeo'] == $country['code'] ?  'selected' : '') ?>  value="<?= $country['code'] ?>"><?= $country['code']; ?>  - <?= $country['name']; ?> [<?= $country['currency']; ?>]</option>
        <?php } ?>
    </select>
    <p class="note">Выберите страну и валюту</p>
</div>


<div class="form-group">
    <label for="kmabiz_webmasterapi"> WEBMASTER API </label><br>
    <input id="kmabiz_webmasterapi" type="text" name="kmabiz_webmasterapi" class="form-control" value="<?php if (isset($saved_settings['kmabiz_webmasterapi'])): echo $saved_settings['kmabiz_webmasterapi']; endif;?>">
    <p class="note">WEBMASTER API - он находится вот по этой ссылке: https://cabinet.kma.biz/user/settings/api-key</p>
</div>

<hr>

<?php if (isset($saved_settings['kmabiz_apigeo']) && $saved_settings['kmabiz_apigeo'] != ''): ?>

    <?php if (!empty($kmabiz_errors)) : ?>
        <?php foreach($kmabiz_errors as $kmabiz_error): ?>
            <div class="alert alert-danger"><?php echo $kmabiz_error ?></div>
        <?php endforeach; ?>
    <?php endif; ?>

    <h3><?php echo $kmabiz_extended_settings; ?></h3>


    <div class="form-group">
        <label for="kmabiz_replace_model">Заменять префикс/постфикс в модели товара</label><br>
        <input id="kmabiz_replace_model" type="text" name="kmabiz_replace_model" class="form-control" value="<?php if (isset($saved_settings['kmabiz_replace_model'])): echo $saved_settings['kmabiz_replace_model']; endif;?>">
        <p class="note">Текст который нужно удалить из модели для синхронизации</p>
    </div>






    <?php if (!empty($new_offers)) : ?>
        <h3>Новые товары</h3>
        <?php echo $new_offers; ?>
    <?php endif; ?>


    <?php if (!empty($similar_offers)) : ?>
        <h3>Товары которые похожи на сайте</h3>
        <?php echo $similar_offers; ?>
    <?php endif; ?>

    <div class="form-group">
        <label for="kmabiz_similar_ignore">ИД оферов которые игнорировать при проверке на совпадения</label><br>
        <input id="kmabiz_similar_ignore" type="text" name="kmabiz_similar_ignore" class="form-control" value="<?php if (isset($saved_settings['kmabiz_similar_ignore'])): echo $saved_settings['kmabiz_similar_ignore']; endif;?>">
        <p class="note">Введите ID оферов через запятую</p>
    </div>



    <?php if (!empty($offers_price_diff)) : ?>
        <h3>Устаревшая цена</h3>
        <?php echo $offers_price_diff; ?>
    <?php endif; ?>


    <div class="form-group">
        <label for="kmabiz_exceptions">ИД оферов которые нужно исключить из рассылки и импорта</label><br>
        <input id="kmabiz_exceptions" type="text" name="kmabiz_exceptions" class="form-control" value="<?php if (isset($saved_settings['kmabiz_exceptions'])): echo $saved_settings['kmabiz_exceptions']; endif;?>">
        <p class="note">Введите ID оферов через запятую</p>
    </div>



    <h3>Импорт</h3>




    <div class="form-group row">
        <label class="col-sm-2 control-label" for="input-parent">Категория для загрузки</label>
        <div class="col-sm-10">
            <input type="text" name="kmabiz_input_category" value="<?php echo $kmabiz_input_category; ?>" placeholder="" id="input-parent" class="form-control" />
            <input type="hidden" name="kmabiz_import_category" value="<?php echo $kmabiz_import_category; ?>" />
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 control-label">Магазины для загрузки</label>
        <div class="col-sm-10">
            <div class="well well-sm" style="height: 150px; overflow: auto;">
                <div class="checkbox">
                    <label>
                        <?php if (in_array(0, $selectedStores)) { ?>
                            <input type="checkbox" name="kmabiz_store[]" value="0" checked="checked" />
                            Основной магазин
                        <?php } else { ?>
                            <input type="checkbox" name="kmabiz_store[]" value="0" />
                            Основной магазин
                        <?php } ?>
                    </label>
                </div>
                <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                        <label>
                            <?php if (in_array($store['store_id'], $selectedStores)) { ?>
                                <input type="checkbox" name="kmabiz_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                                <?php echo $store['name']; ?>
                            <?php } else { ?>
                                <input type="checkbox" name="kmabiz_store[]" value="<?php echo $store['store_id']; ?>" />
                                <?php echo $store['name']; ?>
                            <?php } ?>
                        </label>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="kmabiz_quantity">Количество товара</label><br>
        <input id="kmabiz_quantity" type="text" name="kmabiz_quantity" class="form-control" value="<?php  echo ( isset($saved_settings['kmabiz_quantity']) ? $saved_settings['kmabiz_quantity'] : 9999); ?>">
    </div>

    <script type="text/javascript"><!--
        $('input[name=\'kmabiz_input_category\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        json.unshift({
                            category_id: 0,
                            name: 'Выберите категорию'
                        });

                        response($.map(json, function(item) {
                            return {
                                label: item['name'],
                                value: item['category_id']
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                $('input[name=\'kmabiz_input_category\']').val(item['label']);
                $('input[name=\'kmabiz_import_category\']').val(item['value']);
            }
        });
        //--></script>


<?php endif; ?>
